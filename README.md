# Mailchimp E-Commerce

Mailchimp E-Commerce Async utilizes the Mailchimp Marketing API 3.x to connect a Drupal Commerce store with Mailchimp. It uses queues to asynchronously send Carts, Customers, Products, Promotions, and Orders to Mailchimp allowing a variety of automations to be configured and sent to customers from Mailchimp.

## Features

This is an extensive rebuild of the project [Mailchimp E-Commerce](https://www.drupal.org/project/mailchimp_ecommerce). This version was built with the following objectives:
1. Make sure the user is never interrupted by a failed Mailchimp API call by moving API calls into Queues.
2. Allow as much flexibility as possible through Drupal Admin UI. This includes:
   - Creating, updating and deleting a Mailchimp Store.
   - User defined mappings between Commerce Product and Variation fields and their Mailchimp counterparts.
   - User defined mappings between Order Workflow Transitions and Mailchimp States.
3. Expand the functionality of the module beyond Cart automations and attempt to support as many use-cases as possible.
4. Make use of the first-party Mailchimp Marketing API library to decrease technical debt.

## Post-Installation

Go to the Store Settings (/admin/config/services/mailchimp-ecommerce) and add your Mailchimp API key.

Click on "Create a new store in Mailchimp" and create a Mailchimp Store to sync with Commerce. The store values will be pre-populated with details from your default Commerce Store configuration. The module does not currently support multiple stores.

If you already have a Store in Mailchimp, you can optionally go to "Update Mailchimp connected store" and make any necessary changes. The values in this form are pre-populated from a Mailchimp get Store request.

Once the store is set up, define a Product Property Map at /admin/config/services/mailchimp-ecommerce/product-property-map. Fields that are programmatically defined by Commerce (like title and sku) are not provided. Other product fields from Mailchimp are available for users to select from their product type, or, if they choose, override with a value from the default variation. Product variation types have all the same mappings available as a product type, with the addition of an inventory quantity mapping. @TODO Media and Commerce Stock support.

Once your product property map is defined, you can sync all of your products to Mailchimp at /admin/config/services/mailchimp-ecommerce/product-sync.

Once you have a connected store with products, you can begin sending Cart automations.

@TODO Continue documentation for Order Automations, Promos, and Customers.

## Additional Requirements

This module requires [Commerce](https://www.drupal.org/project/commerce) and [Mailchimp Marketing PHP](https://github.com/mailchimp/mailchimp-marketing-php). This PHP library is maintained and provided by Mailchimp, as opposed to the library maintained by ThinkShout utilized by the Mailchimp and Mailchimp E-Commerce modules. Dependencies will be installed via composer.

## Recommended modules/libraries

Since this module uses a different API library, it is intentionally decoupled it from the [Mailchimp](https://www.drupal.org/project/mailchimp) module; however, that module is recommended for concurrent use as it provides a different set of features. Note that authentication is not shared between Mailchimp and Mailchimp E-Commerce Async modules.

## Similar projects

Mailchimp E-Commerce Async is forked from Mailchimp E-Commerce, which is officially sponsored by Mailchimp.

Ideally, this sandbox module is a suggestion for how a future branch of Mailchimp E-Commerce might function, and could be merged back into the sponsored module.

## Other Notes

Mailchimp timezone codes do not match Drupal and I have not been able to find documentation. The API docs have an example that uses "Eastern" for America/New_York (GMT-5).