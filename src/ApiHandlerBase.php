<?php
declare(strict_types=1);

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\file\Entity\File;
use Drupal\mailchimp_ecommerce_async\Contracts\ApiHandlerBaseInterface;
use Drupal\media\MediaInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;
use MailchimpMarketing\ApiClient;

/**
 * {@inheritDoc}
 */
class ApiHandlerBase implements ApiHandlerBaseInterface {

  /**
   * Use a simple hash to create a unique ID from an email address.
   */
  protected const CUSTOMER_SALT = 'mc';

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Logger Factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $log;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The API Key loaded from site configuration.
   *
   * @var string
   */
  protected string $apiKey;

  /**
   * The Store ID from site configuration.
   *
   * @var string
   */
  protected string $storeId;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected $config;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected $propertyMap;

  /**
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected TranslationInterface $stringTranslation;

  /**
   * The API Client.
   *
   * @var \Drupal\mailchimp_ecommerce_async\MailchimpMarketingApiClient
   */
  protected MailchimpMarketingApiClient $api;

  /**
   * Constructs the service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   * @throws \MailchimpMarketing\ApiException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->log = $logger_factory->get('mailchimp_ecommerce');
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->apiKey = $config_factory->get('mailchimp_ecommerce_async.settings')->get('api_key') ?? '';
    $this->storeId = $config_factory->get('mailchimp_ecommerce_async.settings')->get('store_id') ?? '';
    $this->config = $config_factory->get('mailchimp_ecommerce_async.settings');
    $this->propertyMap = $config_factory->get('mailchimp_ecommerce_async.product_property_map');
    if ($this->apiKey !== '') {
      $this->api = new MailchimpMarketingApiClient($this->apiKey);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function isLibraryInstalled(): bool {
    return class_exists(ApiClient::class);
  }

  /**
   * {@inheritDoc}
   */
  public function isApiKeyValid(?string $api_key = NULL): bool {
    $response = FALSE;
    $this_api = $this->api;
    if (!empty($api_key)) {
      $this_api = $this->getNewApiObject($api_key);
    }

    try {
      $response = (bool) $this_api->ping->get();
    }
    catch (\Exception $e) {
      $this->messenger->addError(
        $this->stringTranslation->translate('Mailchimp Ecommerce: %message', ['%message' => $e->getMessage()]));
    }
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function getApiObject(bool $reset = FALSE, string $api_key = ''): bool|ApiClient {
    $api =& drupal_static(__FUNCTION__, NULL);
    if ($api === '' || $reset || $api_key) {
      $api = $this->getNewApiObject($api_key);
    }
    return $api;
  }

  /**
   * {@inheritDoc}
   */
  public function getNewApiObject(string $api_key = ''): mixed {
    if (!$this->isLibraryInstalled()) {
      $msg = $this->stringTranslation
        ->translate('Failed to load Mailchimp Marketing PHP library. Please refer to the installation requirements.');
      $this->log->error($msg);
      $this->messenger->addError($msg);
      return NULL;
    }

    if ($api_key === '') {
      $api_key = (string) $this->apiKey;
    }

    if (empty($api_key)) {
      $msg = $this->stringTranslation
        ->translate('Failed to load Mailchimp Marketing API Key. Please check your Mailchimp settings.');
      $this->log->error($msg);
      $this->messenger->addError($msg);
      return FALSE;
    }
    // We allow the class name to be overridden, following the example of core's
    // mailsystem, in Order to use alternate Mailchimp Marketing classes.
//    $class_name = $this->configFactory->get('mailchimp.settings')->get('api_classname') ?? MailchimpMarketingApiClient::class;
    $class_name = MailchimpMarketingApiClient::class;
    return new $class_name($api_key);
  }

  public function setNewApiObject(string $api_key) {
    $this->apiKey = $api_key;
    $this->api = $this->getNewApiObject($api_key);
  }
  /**
   * {@inheritDoc}
   */
  public function buildCustomerBody(string $mail, bool $opt_in = FALSE, ProfileInterface $profile = NULL, ?array $property_overrides = NULL): array {
    $body = [
      'id' => $this->createIdFromMail($mail),
      'email_address' => $mail,
      'opt_in_status' => $opt_in,
    ];

    $users = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties([
        'mail' => $mail,
      ]);
    $user = $users ? reset($users) : NULL;

    if ($user instanceof UserInterface) {
      $body['updated_at'] = date('c', (int) $user->getChangedTime());
      $body['created_at'] = date('c', (int) $user->getCreatedTime());
    }

    if ($profile !== NULL) {
      try {
        $address = $profile->get('address')->first();
        if ($address instanceof AddressItem) {
          $body['company'] = $address->getOrganization();
          $body['first_name'] = $address->getGivenName();
          $body['last_name'] = $address->getFamilyName();
          $body['address'] = [
            'address1' => $address->getAddressLine1(),
            'address2' => $address->getAddressLine2(),
            'city' => $address->getLocality(),
            'province' => $address->getAdministrativeArea(),
//            'province_code' => '',
            'postal_code' => $address->getPostalCode(),
//            'country' => '',
            'country_code' => $address->getCountryCode(),
          ];
        }
      }
      catch (MissingDataException $e) {
        // Could not get the address from the profile. Suppress error and skip.
      }
    }

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }
    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function getPropertyMapping(string $entity_type_id, string $bundle_id, string $mailchimp_property_name): mixed {
    return $this->propertyMap->get($entity_type_id . '.' . $bundle_id . '.' . $mailchimp_property_name);
  }

  /**
   * {@inheritDoc}
   */
  public function getMappedPropertyValue(ProductVariationInterface|ProductInterface $product, string $mailchimp_property_name): mixed {
    $field = NULL;
    $map = $this->getPropertyMapping($product->getEntityTypeId(), $product->bundle(), $mailchimp_property_name);
    if ($product instanceof ProductInterface) {
      if ($map['use_variation'] === FALSE) {
        $field = $product->hasField($map['drupal_field']) ? $product->get($map['drupal_field']) : NULL;
      }
      else {
        $product = $product->getDefaultVariation() ?: $product->getVariations()[0];
        $map = $this->getPropertyMapping($product->getEntityTypeId(), $product->bundle(), $mailchimp_property_name);
      }
    }

    if ($mailchimp_property_name === 'inventory_quantity' && $map === 'non_inventory') {
      // Mailchimp requires an inventory quantity, if the product/variation type
      // does not use inventory, set it to an arbitrarily high number.
      // @TODO what if inventory/non-inventory is a configurable within the product type?
      return '99999';
    }

    if ($product instanceof ProductVariationInterface) {
      $field = $product->hasField($map) ? $product->get($map) : NULL;
    }

    if ($field instanceof EntityReferenceFieldItemListInterface) {
      $entity = $field->referencedEntities();
      $entity = is_array($entity) ? array_pop($entity) : $entity;

      if ($entity instanceof MediaInterface) {
        $entity_id = $entity->getSource()?->getSourceFieldValue($entity);
        $entity = $this->entityTypeManager->getStorage('file')->load($entity_id);
      }

      if ($entity instanceof File) {
        return $entity->createFileUrl(FALSE);
      }
    }

    if ($field instanceof FieldItemListInterface) {
      return $field->value;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function createIdFromMail(string $mail): string {
    return preg_replace('/[^a-zA-Z0-9]/', '', crypt(strtolower($mail), self::CUSTOMER_SALT));
  }

  /**
   * {@inheritDoc}
   */
  public function validateCampaignId(string $campaign_id): bool {
    try {
      $this->api->campaigns->get($campaign_id);
    }
    catch (\Exception $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      if ($e->getCode() !== 404) {
        $this->log($e, 'Unable to validate campaign ID.');
      }
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Quick exception log helper.
   *
   * @param \Exception $e
   *   The exception.
   * @param string $additional_message
   *   A message to add to the log.
   */
  protected function log(\Exception $e, string $additional_message = ''): void {
    $additional_message .= $additional_message !== '' ? '<br> ' : '';
    \Drupal::logger('mailchimp_ecommerce')
      ->error($additional_message .
        $e->getCode() . '<br> ' .
        $e->getMessage() . '<br> ' .
        '<pre>' . print_r($e->getTrace(), TRUE) . '</pre>'
      );
  }
}
