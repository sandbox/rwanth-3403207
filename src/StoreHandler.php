<?php

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * {@inheritDoc}
 */
class StoreHandler extends ApiHandlerBase implements StoreHandlerInterface {

  /**
   * {@inheritDoc}
   */
  public function getStores(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?\stdClass {
    try {
      return $this->api->ecommerce->stores($fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      $this->log($e);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getStore(string $remote_store_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getStore($remote_store_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      $this->log($e);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addStore(array $store_body): ?\stdClass {
    try {
      return $this->api->ecommerce->addStore($store_body);
    }
    catch (RequestException $e) {
      $this->log($e);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function updateStore(string $remote_store_id, array $body): ?\stdClass {
    try {
      return $this->api->ecommerce->updateStore($remote_store_id, $body);
    }
    catch (RequestException $e) {
      $this->log($e);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteStore(string $remote_store_id): void {
    try {
      $this->api->ecommerce->deleteStore($remote_store_id);
    }
    catch (RequestException $e) {
      $this->log($e);
    }
  }

  /**
   * Store creation needs to get all lists from Mailchimp.
   */
  public function getLists() {
    try {
      return $this->api->lists->getAllLists();
    }
    catch (RequestException $e) {
      $this->log($e);
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function enableSyncing(): void {
    try {
      $this->api->ecommerce->updateStore($this->storeId, [
        'is_syncing' => TRUE,
      ]);
    }
    catch (RequestException $e) {
      $this->log($e);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function disableSyncing(): void {
    try {
      $this->api->ecommerce->updateStore($this->storeId, [
        'is_syncing' => FALSE,
      ]);
    }
    catch (RequestException $e) {
      $this->log($e);
    }
  }

}
