<?php

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Url;
use Drupal\mailchimp_ecommerce_async\Contracts\OrderHandlerInterface;
use GuzzleHttp\Exception\RequestException;
use stdClass;

/**
 * {@inheritDoc}
 */
class OrderHandler extends ApiHandlerBase implements OrderHandlerInterface {

  /**
   * {@inheritDoc}
   */
  public function syncCart(int|string $cart_id, ?string $campaign_id = NULL): void {
    $property_overrides = [];

    if (!empty($campaign_id) && $this->validateCampaignId($campaign_id)) {
      $property_overrides = ['campaign_id' => $campaign_id];
    }

    try {
      $mc_cart = $this->getCart($cart_id);

      if (!is_null($mc_cart)) {
        // Get cart succeeded, we can now update it.
        $this->updateCart($cart_id, $property_overrides);
      }
      else {
        // Get cart did not find a cart in Mailchimp, create it now.
        $this->addCart($cart_id, $property_overrides);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync cart failed for order %id.', ['%id' => $cart_id])
      );
    }
    catch (EntityStorageException $entityStorageException) {
      $this->log($entityStorageException,
        $this->stringTranslation->translate('Sync cart failed because order %id was deleted in Drupal before this queue item was processed.', ['%id' => $cart_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function syncOrder(int|string $order_id, ?string $mc_state = NULL, ?string $campaign_id = NULL): void {
    $property_overrides = [];
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    if ($order instanceof OrderInterface) {
      if ($mc_state === 'shipped') {
        $property_overrides['fulfillment_status'] = $mc_state;
      }
      elseif ($mc_state === 'cancelled') {
        $property_overrides['financial_status'] = $mc_state;
        $property_overrides['cancelled_at_foreign'] = date('c', (int)$order->getChangedTime());
      }
      else {
        $property_overrides['financial_status'] = $mc_state;
      }
      $completed = $order->getCompletedTime();
      if (!empty($completed)) {
        $property_overrides['processed_at_foreign'] = date('c', (int)$completed);
      }

      if (!empty($campaign_id) && $this->validateCampaignId($campaign_id)) {
        $property_overrides['campaign_id'] = $campaign_id;
      }

      try {
        $mc_order = $this->getOrder($order_id);

        if ($mc_order !== NULL) {
          // Get order succeeded, we can now update it.
          $this->updateOrder($order_id, $property_overrides);
        }
        else {
          // Get order did not find an order in Mailchimp, create it now.
          $this->addOrder($order_id, $property_overrides);
        }
      }
      catch (RequestException $e) {
        if ($e->getCode() === 429 || $e->getCode() >= 500) {
          throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
        }
        $this->log($e,
          $e->getResponse()?->getBody()
        );
      }
      catch (EntityStorageException $entityStorageException) {
        $this->log($entityStorageException,
          $this->stringTranslation->translate('Sync order failed because order %id was deleted in Drupal before this queue item was processed.', ['%id' => $order_id])
        );
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCarts(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass {
    try {
      return $this->api->ecommerce->getStoreCarts($this->storeId, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCart(int|string $cart_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass {
    try {
      return $this->api->ecommerce->getStoreCart($this->storeId, $cart_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCartLines(int|string $cart_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass {
    try {
      return $this->api->ecommerce->getAllCartLineItems($this->storeId, $cart_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCartLine(int|string $cart_id, int|string $line_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass {
    try {
      return $this->api->ecommerce->getCartLineItem($this->storeId, $cart_id, $line_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getOrders(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass {
    try {
      return $this->api->ecommerce->getStoreOrders($this->storeId, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getOrder(int|string $order_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass {
    try {
      return $this->api->ecommerce->getOrder($this->storeId, $order_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getOrderLines(int|string $order_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass {
    try {
      return $this->api->ecommerce->getAllOrderLineItems($this->storeId, $order_id, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getOrderLine(int|string $order_id, int|string $line_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass {
    try {
      return $this->api->ecommerce->getOrderLineItem($this->storeId, $order_id, $line_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addCart(int|string $cart_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildCartBody($cart_id, $property_overrides);
    return $this->api->ecommerce->addStoreCart($this->storeId, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function addCartLine(int|string $cart_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildCartLineBody($line_id, $property_overrides);
    return $this->api->ecommerce->addCartLineItem($this->storeId, $cart_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function addOrder(int|string $order_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildOrderBody($order_id, $property_overrides);
    return $this->api->ecommerce->addStoreOrder($this->storeId, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function addOrderLine(int|string $order_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildOrderLineBody($line_id, $property_overrides);
    return $this->api->ecommerce->addOrderLineItem($this->storeId, $order_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updateCart(int|string $cart_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildCartBody($cart_id, $property_overrides);
    return $this->api->ecommerce->updateStoreCart($this->storeId, $cart_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updateCartLine(int|string $cart_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildCartLineBody($line_id, $property_overrides);
    return $this->api->ecommerce->updateCartLineItem($this->storeId, $cart_id, $line_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updateOrder(int|string $order_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildOrderBody($order_id, $property_overrides);
    return $this->api->ecommerce->updateOrder($this->storeId, $order_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updateOrderLine(int|string $order_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass {
    $body = $this->buildOrderLineBody($line_id, $property_overrides);
    return $this->api->ecommerce->updateOrderLineItem($this->storeId, $order_id, $line_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCart(int|string $cart_id): void {
    try {
      $this->api->ecommerce->deleteStoreCart($this->storeId, $cart_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete cart failed for order %id.', ['%id' => $cart_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCartLine(int|string $cart_id, int|string $line_id): void {
    try {
      $this->api->ecommerce->deleteCartLineItem($this->storeId, $cart_id, $line_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete cart line failed for order %id.', ['%id' => $cart_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteOrder(int|string $order_id): void {
    try {
      $this->api->ecommerce->deleteOrder($this->storeId, $order_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete order failed for order %id.', ['%id' => $order_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteOrderLine(int|string $order_id, int|string $line_id): void {
    try {
      $this->api->ecommerce->deleteOrderLineItem($this->storeId, $order_id, $line_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete order line failed for order %id.', ['%id' => $order_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildCartOrderBase(?OrderInterface $order = NULL): array {
    if ($order === NULL) {
      throw new EntityStorageException('The order no longer exists.');
    }

    $mail = $order->getEmail();
    if (empty($mail)) {
      return [];
    }

    $price = $order->getTotalPrice();

    $total_tax = 0;
    $taxes = $order->collectAdjustments(['tax']);
    foreach ($taxes as $tax) {
      $total_tax += (float)$tax?->getAmount()?->getNumber();
    }

    $profile = $order->getBillingProfile();
    $opt_in = $order->getData('mc_checkout_subscribe', FALSE);

    return [
      'id' => $order->id(),
      'customer' => $this->buildCustomerBody($mail, $opt_in, $profile),
      'currency_code' => $price?->getCurrencyCode(),
      'order_total' => (float)$price?->getNumber(),
      'lines' => [],
      'tax_total' => round($total_tax, 2),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildCartOrderLineBase(?OrderItemInterface $order_item): array {
    if ($order_item === NULL) {
      throw new EntityStorageException('The order item does not exist.');
    }

    $id = $order_item->id();
    $product_variation = $order_item->getPurchasedEntity();

    return [
      'id' => $id,
      'product_id' => (string)$product_variation?->getProductId(),
      'product_variant_id' => (string)$order_item->getPurchasedEntityId(),
      'quantity' => (int)$order_item->getQuantity(),
      'price' => (float)$order_item->getUnitPrice()?->getNumber(),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildCartBody(OrderInterface|int|string $cart, ?array $property_overrides = NULL): array {
    if (is_string($cart) || is_int($cart)) {
      $cart = $this->entityTypeManager
        ->getStorage('commerce_order')
        ->load($cart);
    }
    // Allow exceptions thrown here to bubble up.
    $body = $this->buildCartOrderBase($cart);

    $body['checkout_url'] = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $cart->id()], ['absolute' => TRUE])->toString();

    $lines = [];
    foreach ($cart->getItems() as $order_item) {
      $lines[] = $this->buildCartOrderLineBase($order_item);
    }
    $body['lines'] = $lines;
    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }
    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function buildCartLineBody(OrderItemInterface|int|string $line, ?array $property_overrides = NULL): array {
    if (is_string($line) || is_int($line)) {
      $line = $this->entityTypeManager
        ->getStorage('commerce_order_item')
        ->load($line);
    }

    $body = $this->buildCartOrderLineBase($line);

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }

    return $body;
  }


  /**
   * {@inheritDoc}
   */
  public function buildOrderBody(OrderInterface|int|string $order, ?array $property_overrides = NULL): array {
    if (is_string($order) || is_int($order)) {
      $order = $this->entityTypeManager
        ->getStorage('commerce_order')
        ->load($order);
    }

    // Allow exceptions thrown here to bubble up.
    $body = $this->buildCartOrderBase($order);

    // Order lines.
    $lines = [];
    foreach ($order->getItems() as $order_item) {
      $lines[] = $this->buildOrderLineBody($order_item);
    }
    $body['lines'] = $lines;

    // Shipping total.
    $total_shipping = 0;
    $shipping = $order->collectAdjustments(['shipping']);
    foreach ($shipping as $ship) {
      $total_shipping += (float)$ship?->getAmount()?->getNumber();
    }
    $body['shipping_total'] = round($total_shipping, 2);

    // Order URL.
    $user_id = $order->getCustomerId();
    if ($user_id !== 0) {
      $body['order_url'] =
        Url::fromRoute(
          'entity.commerce_order.user_view',
          [
            'user' => $user_id,
            'commerce_order' => $order->id(),
          ],
          ['absolute' => TRUE]
        )
          ->toString();
    }

    // Landing site.
    $body['landing_site'] = Url::fromUri('internal:/', ['absolute' => TRUE])->toString();

    // Line discount.
    $total_discount = 0;
    $discounts = $order->collectAdjustments();
    foreach ($discounts as $discount) {
      $number = (float)$discount?->getAmount()?->getNumber();
      $number >= 0 ?: $total_discount += $number;
    }
    $body['discount_total'] = $total_discount;

    // Updated at foreign times.
    $body['updated_at_foreign'] = date('c', (int)$order->getChangedTime());

    // @TODO assign these.
    //    $body['tracking_code'] = '';
    //    $body['shipping_address'] = [];
    //    $body['billing_address'] = [];
    //    $body['promos'] = [];
    //    $body['outreach'] = [];
    //    $body['tracking_number'] = '';
    //    $body['tracking_carrier'] = '';
    //    $body['tracking_url'] = '';

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }
    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOrderLineBody(OrderItemInterface|int|string $order_item, ?array $property_overrides = NULL): array {
    if (is_string($order_item) || is_int($order_item)) {
      $order_item = $this->entityTypeManager
        ->getStorage('commerce_order_item')
        ->load($order_item);
    }
    $body = $this->buildCartOrderLineBase($order_item);

    // Line discount.
    $line_discount = 0;
    $discounts = $order_item->getAdjustments();
    foreach ($discounts as $discount) {
      $number = (float)$discount?->getAmount()?->getNumber();
      $number >= 0 ?: $line_discount += $number;
    }

    $body['discount'] = round($line_discount, 2);
    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }
    return $body;
  }

}
