<?php

namespace Drupal\mailchimp_ecommerce_async\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates Orders in Mailchimp.
 *
 * @QueueWorker(
 *   id = "mailchimp_ecommerce_async_product_queue",
 *   title = @Translation("Mailchimp Ecommerce: Product Queue"),
 *   cron = {"time" = 120}
 * )
 */
class ProductQueue extends QueueWorkerBase {

  /**
   * API handler service for products.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\ProductHandlerInterface
   */
  protected $productHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->productHandler = \Drupal::service('mailchimp_ecommerce_async.product_handler');
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {
    $product_events = [
      'productInsert',
      'productUpdate',
      'productDelete',
    ];

    $variation_events = [
      'variationInsert',
      'variationUpdate',
      'variationDelete',
    ];

    if (in_array($data['event'], $product_events, TRUE)) {
      switch ($data['event']) {
        case 'productInsert':
        case 'productUpdate':
          $this->productHandler->syncProduct($data['product_id']);
          break;

        case 'productDelete':
          $this->productHandler->deleteProduct($data['product_id']);
          break;
      }
    }

    elseif (in_array($data['event'], $variation_events, TRUE)) {
      // Sometimes the product ID passed from the event subscriber is null.
      // If this is the case, try to repopulate it now.
      if ($data['product_id'] === NULL) {
        $variation = \Drupal::entityTypeManager()
          ->getStorage('commerce_product_variation')
          ->load($data['variation_id']);
        $data['product_id'] = $variation?->getProductId();
      }

      // If product is still null, the variation is orphaned. Do not sync.
      if ($data['product_id'] !== NULL) {
        switch ($data['event']) {
          case 'variationInsert':
          case 'variationUpdate':
            // Product sync always includes rebuilding and sending variations
            // as part of the product body. Since variations also depend on the
            // existence of a product, it is safe to just re-sync the entire
            // product on any variation insert or update.
            $this->productHandler->syncProduct($data['product_id']);
            break;

          case 'variationDelete':
            $this->productHandler->deleteProductVariant($data['product_id'], $data['variation_id']);
            break;
        }
      }
    }
  }

}
