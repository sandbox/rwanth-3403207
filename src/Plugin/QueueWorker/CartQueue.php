<?php

namespace Drupal\mailchimp_ecommerce_async\Plugin\QueueWorker;

use Drupal\commerce_cart\Event\CartEmptyEvent;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates Orders in Mailchimp.
 *
 * @QueueWorker(
 *   id = "mailchimp_ecommerce_async_cart_queue",
 *   title = @Translation("Mailchimp Ecommerce Cart Add/Update"),
 *   cron = {"time" = 120}
 * )
 */
class CartQueue extends QueueWorkerBase {

  /**
   * API handler service for orders and carts.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\OrderHandlerInterface
   */
  protected $orderHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->orderHandler = \Drupal::service('mailchimp_ecommerce_async.order_handler');
  }

  /**
   * Process the changes made to a cart.
   *
   * @param mixed $data
   *   Data that was passed to QueueInterface::createItem() when queued.
   *   This is an array containing the Order id, email, event type, and order
   *   item id.
   *
   *   {@inheritDoc}.
   */
  public function processItem($data) : void {
    $order_exists = \Drupal::entityQuery('commerce_order')
      ->condition('order_id', $data['order_id'])
      ->accessCheck(FALSE)
      ->execute();

    if ($order_exists !== []) {
      switch ($data['event']) {
        case 'orderPlace':
          // To transition cart to order, first delete the Mailchimp cart.
          // The order will be created in the order queue when the transition to "Financial Status: Pending" occurs,
          // as defined by the Order Workflow Map configuration.
          $this->orderHandler->deleteCart($data['order_id']);
          break;

        case CartEmptyEvent::class:
        case 'orderDelete':
          $this->orderHandler->deleteCart($data['order_id']);
          break;

        default:
          // All other cart events.
          $this->orderHandler->syncCart($data['order_id'],  $data['campaign_id']);
      }
    }
  }

}
