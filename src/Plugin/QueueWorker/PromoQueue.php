<?php

namespace Drupal\mailchimp_ecommerce_async\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates Promo Codes and Rules in Mailchimp.
 *
 * @QueueWorker(
 *   id = "mailchimp_ecommerce_async_promo_queue",
 *   title = @Translation("Mailchimp Ecommerce: Promo Queue"),
 *   cron = {"time" = 120}
 * )
 */
class PromoQueue extends QueueWorkerBase {

  /**
   * API handler service for products.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\PromoHandlerInterface
   */
  protected $promoHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->promoHandler = \Drupal::service('mailchimp_ecommerce_async.promo_handler');
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {
    switch ($data['event']) {
      case 'promotionInsert':
      case 'promotionUpdate':
        $this->promoHandler->syncPromoRule($data['promo_rule_id']);
        break;

      case 'promotionDelete':
        $this->promoHandler->deletePromoRule($data['promo_rule_id']);
        break;

      case 'couponInsert':
      case 'couponUpdate':
        $this->promoHandler->syncPromoCode($data['promo_rule_id'], $data['promo_code_id']);
        break;

      case 'couponDelete':
        $this->promoHandler->deletePromoCode($data['promo_rule_id'], $data['promo_code_id']);
        break;
    }
  }
}
