<?php

namespace Drupal\mailchimp_ecommerce_async\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates Orders in Mailchimp.
 *
 * @QueueWorker(
 *   id = "mailchimp_ecommerce_async_order_queue",
 *   title = @Translation("Mailchimp Ecommerce Orders"),
 *   cron = {"time" = 120}
 * )
 */
class OrderQueue extends QueueWorkerBase {

  /**
   * API handler service for orders and carts.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\OrderHandlerInterface
   */
  protected $orderHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->orderHandler = \Drupal::service('mailchimp_ecommerce_async.order_handler');
  }

  /**
   * Attempt to process the queue item. If the order doesn't exist, that means
   * there is a delete operation later in the queue, so do nothing.
   */
  public function processItem($data) {
    $order_exists = \Drupal::entityQuery('commerce_order')
      ->condition('order_id', $data['order_id'])
      ->accessCheck(FALSE)
      ->execute();

    if ($order_exists !== []) {
      $this->orderHandler->syncOrder($data['order_id'], $data['state'], $data['campaign_id']);
    }
  }

}
