<?php

namespace Drupal\mailchimp_ecommerce_async\Plugin\QueueWorker;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates Orders in Mailchimp.
 *
 * @QueueWorker(
 *   id = "mailchimp_ecommerce_async_customer_queue",
 *   title = @Translation("Mailchimp Ecommerce Customer Subscribe"),
 *   cron = {"time" = 120}
 * )
 */
class CustomerQueue extends QueueWorkerBase {

  /**
   * API handler service for customers.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\CustomerHandlerInterface
   */
  protected $customerHandler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->customerHandler = \Drupal::service('mailchimp_ecommerce_async.customer_handler');
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {
    $mail = $data['email'] ?? Order::load($data['order_id'])?->getEmail();
    if (\Drupal::configFactory()->get('mailchimp_ecommerce_async.settings')
      ?->get('double_opt_in')) {
      $this->customerHandler->syncCustomerDoubleOptIn($mail);
    }
    else {
      $this->customerHandler->syncCustomer($mail, TRUE);
    }
  }

}
