<?php

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\mailchimp_ecommerce_async\Contracts\ProductHandlerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * {@inheritDoc}
 */
class ProductHandler extends ApiHandlerBase implements ProductHandlerInterface {

  /**
   * {@inheritDoc}
   */
  public function syncProduct(int|string $product_id): void {
    try {
      $mc_product = $this->getProduct($product_id);

      if (!is_null($mc_product)) {
        // Get product succeeded, we can now update it.
        $this->updateProduct($product_id);
      }
      else {
        // Get product did not find a product in Mailchimp, create it now.
        $this->addProduct($product_id);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync product failed for product %id.', ['%id' => $product_id])
      );
    }
    catch (EntityStorageException $entityStorageException) {
      $this->log($entityStorageException,
        $this->stringTranslation->translate('Sync product failed because product %id was deleted in Drupal before this queue item was processed.', ['%id' => $product_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getProducts(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?\stdClass {
    try {
      return $this->api->ecommerce->getAllStoreProducts($this->storeId, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getProduct(int|string $product_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getStoreProduct($this->storeId, $product_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getProductVariants(int|string $product_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?\stdClass {
    try {
      return $this->api->ecommerce->getProductVariants($this->storeId, $product_id, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getProductVariant(int|string $product_id, int|string $variant_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getProductVariant($this->storeId, $product_id, $variant_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addProduct(int|string $product_id): ?\stdClass {
    $product = $this->buildProductBody($product_id);
    return $this->api->ecommerce->addStoreProduct($this->storeId, $product);
  }

  /**
   * {@inheritDoc}
   */
  public function addProductVariant(int|string $product_id, int|string $variant_id): ?\stdClass {
    $variant = $this->buildProductVariantBody($variant_id);
    return $this->api->ecommerce->addProductVariant($this->storeId, $product_id, $variant_id, $variant);
  }

  /**
   * {@inheritDoc}
   */
  public function updateProduct(int|string $product_id): ?\stdClass {
    $product = $this->buildProductBody($product_id);
    return $this->api->ecommerce->updateStoreProduct($this->storeId, $product_id, $product);
  }

  /**
   * {@inheritDoc}
   */
  public function updateProductVariant(int|string $product_id, int|string $variant_id): ?\stdClass {
    $variant = $this->buildProductVariantBody($variant_id);
    return $this->api->ecommerce->updateProductVariant($this->storeId, $product_id, $variant_id, $variant);

  }

  /**
   * {@inheritDoc}
   */
  public function deleteProduct(int|string $product_id): void {
    try {
      $this->api->ecommerce->deleteStoreProduct($this->storeId, $product_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete product failed for product %pid.', [
          '%pid' => $product_id
        ])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteProductVariant(int|string $product_id, int|string $variant_id): void {
    try {
      $this->api->ecommerce->deleteProductVariant($this->storeId, $product_id, $variant_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete variation failed for product %pid variation %vid.', [
          '%pid' => $product_id,
          '%vid' => $variant_id,
        ])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildProductBody(string $product_id, ?array $property_overrides = NULL): array {
    $commerce_product = $this->entityTypeManager
      ->getStorage('commerce_product')
      ->load($product_id);
    if (!($commerce_product instanceof ProductInterface)) {
      throw new EntityStorageException('Product '. $product_id . ' no longer exists in Drupal');
    }
    $variation_ids = $commerce_product->getVariationIds();
    $mailchimp_variations = [];
    foreach ($variation_ids as $variation_id) {
      $mailchimp_variations[] = $this->buildProductVariantBody($variation_id);
    }

    $body = [
      'id' => $product_id,
      'title' => $commerce_product->getTitle(),
      'variants' => $mailchimp_variations,
//      'handle' => '',
      'type' => $commerce_product->getEntityTypeId(),
//      'vendor' => '',
//      'images' => [],
      'published_at_foreign' => date('c', (int) $commerce_product->getChangedTime()),
    ];

    if ($description = $this->getMappedPropertyValue($commerce_product, 'description')) {
      $body['description'] = $description;
    }

    if ($image_url = $this->getMappedPropertyValue($commerce_product, 'image_url')) {
      $body['image_url'] = $image_url;
    }

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }

    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function buildProductVariantBody(string $variation_id, ?array $property_overrides = NULL): array {
    $variation = $this->entityTypeManager
      ->getStorage('commerce_product_variation')
      ->load($variation_id);
    if (!($variation instanceof ProductVariationInterface)) {
      throw new EntityStorageException('Product Variation '. $variation_id . ' no longer exists in Drupal');
    }
    // @TODO Support for Commerce stock.
    $body = [
      'id' => $variation->id(),
      'title' => $variation->getTitle(),
      'url' => $variation->toUrl('canonical', ['absolute' => TRUE])->toString(),
      'sku' => $variation->getSku(),
      'price' => $variation->getPrice() ? $variation->getPrice()->getNumber() : 0,
//      'backorders' => '',
      'visibility' => $variation->isPublished() ? 'true' : 'false',
    ];

    if ($inventory_quantity = $this->getMappedPropertyValue($variation, 'inventory_quantity')) {
      $body['inventory_quantity'] = (int) $inventory_quantity;
    }
    if ($image_url = $this->getMappedPropertyValue($variation, 'image_url')) {
      $body['image_url'] = $image_url;
    }

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }

    return $body;
  }

}
