<?php

declare(strict_types=1);
namespace Drupal\mailchimp_ecommerce_async;

use MailchimpMarketing\ApiClient;
use MailchimpMarketing\ApiException;

/**
 * Overrides default Mailchimp Transactional library.
 *
 * Intercepts API responses and makes sure ApiException is thrown.
 */
class MailchimpMarketingApiClient extends ApiClient {

  /**
   * Timeout in seconds for requests to the Mailchimp Transactional API.
   *
   * @var int
   */
  protected $timeout;

  /**
   * Override constructor to remove curl operations.
   *
   * @throws \MailchimpMarketing\ApiException
   */
  public function __construct(string $apikey = '', string $server = '', $timeout = 60) {
    parent::__construct();

    if (!$apikey) {
      throw new ApiException('You must provide a Mailchimp API key');
    }

    if ($server === '') {
      $server = explode("-", $apikey)[1];
    }

    $this->setConfig([
      'apiKey' => $apikey,
      'server' => $server,
    ]);

    $this->timeout = $timeout;
    $this->host = rtrim($this->host, '/');
  }

  /**
   * Override __destruct() to prevent calling curl_close().
   */
  public function __destruct() {}

}
