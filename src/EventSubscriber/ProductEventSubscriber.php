<?php

namespace Drupal\mailchimp_ecommerce_async\EventSubscriber;

use Drupal\commerce_product\Event\ProductEvent;
use Drupal\commerce_product\Event\ProductEvents;
use Drupal\commerce_product\Event\ProductVariationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for Commerce Products.
 */
class ProductEventSubscriber extends BaseEventSubscriber {

  private const QUEUE_NAME = 'mailchimp_ecommerce_async_product_queue';

  /**
   * Respond to event fired after saving a new product.
   *
   * @param \Drupal\commerce_product\Event\ProductEvent $event
   */
  public function productInsert(ProductEvent $event) : void {
    $product = $event->getProduct();
    $product_id = $product->id();

    $data = [
      'product_id' => $product_id,
      'event' => 'productInsert',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to event fired after updating an existing product.
   *
   * @param \Drupal\commerce_product\Event\ProductEvent $event
   */
  public function productUpdate(ProductEvent $event) : void {
    $product = $event->getProduct();
    $product_id = $product->id();
    $data = [
      'product_id' => $product_id,
      'event' => 'productUpdate',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to event fired after deleting a product.
   *
   * @param \Drupal\commerce_product\Event\ProductEvent $event
   */
  public function productDelete(ProductEvent $event) : void {
    $product = $event->getProduct();
    $product_id = $product->id();
    $data = [
      'product_id' => $product_id,
      'event' => 'productDelete',
    ];

    $this->deleteQueueItems($this::QUEUE_NAME, 'variation_id', $data['variation_id']);
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to event fired after saving a new variation.
   *
   * @param \Drupal\commerce_product\Event\ProductVariationEvent $event
   */
  public function variationInsert(ProductVariationEvent $event) : void {
    $variation = $event->getProductVariation();
    $product_id = $variation->getProductId();

    $data = [
      'product_id' => $product_id,
      'variation_id' => $variation->id(),
      'event' => 'variationInsert',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to event fired after updating an existing variation.
   *
   * @param \Drupal\commerce_product\Event\ProductVariationEvent $event
   */
  public function variationUpdate(ProductVariationEvent $event) : void {
    $variation = $event->getProductVariation();
    $product_id = $variation->getProductId();

    $data = [
      'product_id' => $product_id,
      'variation_id' => $variation->id(),
      'event' => 'variationUpdate',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to event fired after deleting a variation.
   *
   * @param \Drupal\commerce_product\Event\ProductVariationEvent $event
   */
  public function variationDelete(ProductVariationEvent $event) : void {
    $variation = $event->getProductVariation();
    $product_id = $variation->getProductId();

    $data = [
      'product_id' => $product_id,
      'variation_id' => $variation->id(),
      'event' => 'variationDelete',
    ];

    $this->deleteQueueItems($this::QUEUE_NAME, 'variation_id', $data['variation_id']);
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      ProductEvents::PRODUCT_INSERT => 'productInsert',
      ProductEvents::PRODUCT_UPDATE => 'productUpdate',
      ProductEvents::PRODUCT_DELETE => 'productDelete',
      ProductEvents::PRODUCT_VARIATION_INSERT => 'variationInsert',
      ProductEvents::PRODUCT_VARIATION_UPDATE => 'variationUpdate',
      ProductEvents::PRODUCT_VARIATION_DELETE => 'variationDelete',
    ];
  }

}
