<?php

namespace Drupal\mailchimp_ecommerce_async\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvents;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Event Subscriber for Commerce Orders.
 */
class OrderEventSubscriber extends BaseEventSubscriber {

  private const QUEUE_NAME = 'mailchimp_ecommerce_async_order_queue';

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [
      'commerce_order.place.post_transition' => 'orderUpdate',
      'commerce_order.validate.post_transition' => 'orderUpdate',
      'commerce_order.process.post_transition' => 'orderUpdate',
      'commerce_order.fulfill.post_transition' => 'orderUpdate',
      'commerce_order.cancel.post_transition' => 'orderUpdate',
    ];
    return $events;
  }

  /**
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   */
  public function orderUpdate(WorkflowTransitionEvent $event): void {
    $transition = $event->getTransition()->getId();
    $order = $event->getEntity();
    $map = \Drupal::configFactory()
      ->get('mailchimp_ecommerce_async.order_workflow_map')
      ->getRawData();
    $mapped_state = array_search($transition,
      $map['order_types'][$order->bundle()], TRUE);
    $session = \Drupal::service('request_stack')->getCurrentRequest()->getSession();

    if ($mapped_state) {
      $data = [
        'order_id' => $order->id(),
        'state' => $mapped_state,
        'campaign_id' => $session->get('mc_cid', NULL),
      ];
      $this->createQueueItem($this::QUEUE_NAME, $data);
    }
  }

}
