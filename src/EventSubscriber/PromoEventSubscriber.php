<?php

namespace Drupal\mailchimp_ecommerce_async\EventSubscriber;

use Drupal\commerce_promotion\Event\PromotionEvent;
use Drupal\commerce_promotion\Event\PromotionEvents;
use Drupal\commerce_promotion\Event\CouponEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for Commerce Promotions.
 */
class PromoEventSubscriber extends BaseEventSubscriber {
  private const QUEUE_NAME = 'mailchimp_ecommerce_async_promo_queue';

  public function promotionInsert(PromotionEvent $event) {
    $data = [
      'promo_rule_id' => $event->getPromotion()->id(),
      'event' => 'promotionInsert',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  public function promotionUpdate(PromotionEvent $event) {
    $data = [
      'promo_rule_id' => $event->getPromotion()->id(),
      'event' => 'promotionUpdate',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  public function promotionDelete(PromotionEvent $event) {
    $data = [
      'promo_rule_id' => $event->getPromotion()->id(),
      'event' => 'promotionDelete',
    ];
    $this->deleteQueueItems($this::QUEUE_NAME, 'promo_rule_id', $data['promo_rule_id']);
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  public function couponInsert(CouponEvent $event) {
    $coupon = $event->getCoupon();
    $data = [
      'promo_code_id' => $coupon?->id(),
      'promo_rule_id' => $coupon?->getPromotionId(),
      'event' => 'couponUpdate',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  public function couponUpdate(CouponEvent $event) {
    $coupon = $event->getCoupon();
    $data = [
      'promo_code_id' => $coupon?->id(),
      'promo_rule_id' => $coupon?->getPromotionId(),
      'event' => 'couponUpdate',
    ];
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  public function couponDelete(CouponEvent $event) {
    $coupon = $event->getCoupon();
    $data = [
      'promo_code_id' => $coupon?->id(),
      'promo_rule_id' => $coupon?->getPromotionId(),
      'event' => 'couponDelete',
    ];
    $this->deleteQueueItems($this::QUEUE_NAME, 'promo_code_id', $data['promo_code_id']);
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      PromotionEvents::PROMOTION_INSERT => 'promotionInsert',
      PromotionEvents::PROMOTION_UPDATE => 'promotionUpdate',
      PromotionEvents::PROMOTION_DELETE => 'promotionDelete',
      PromotionEvents::COUPON_INSERT => 'couponInsert',
      PromotionEvents::COUPON_UPDATE => 'couponUpdate',
      PromotionEvents::COUPON_DELETE => 'couponDelete',
    ];
  }

}
