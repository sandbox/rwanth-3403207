<?php

namespace Drupal\mailchimp_ecommerce_async\EventSubscriber;

use Drupal\commerce\EventBase;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for Commerce Carts.
 */
class CartEventSubscriber extends BaseEventSubscriber {
  private const QUEUE_NAME = 'mailchimp_ecommerce_async_cart_queue';

  /**
   * Build the generic data for multiple responses.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $cart
   * @param \Drupal\commerce\EventBase $event
   * @return void
   */
  private function eventResponse(OrderInterface $cart, EventBase $event) {
    $session = \Drupal::service('request_stack')->getCurrentRequest()->getSession();

    $data = [
      'order_id' => $cart->id(),
      'email' => $cart->getEmail(),
      'event' => get_class($event),
      'order_item_id' => NULL,
      'campaign_id' => $session->get('mc_cid', NULL),
      'landing_site' => $session->get('mc_landing_site', NULL),
    ];

    if (method_exists($event, 'getOrderItem')) {
      $data['order_item_id'] = $event->getOrderItem()->id();
    }

    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Respond to various cart events.
   *
   * @param \Drupal\commerce\EventBase $event
   *   A Commerce cart event.
   */
  public function cartEventResponse(EventBase $event) : void {
    $cart = $event->getCart();

    if ($cart->getEmail() !== NULL) {
      $this->eventResponse($cart, $event);
    }
  }

  /**
   * Respond to various Order events (while the Order is still a cart.)
   *
   * @param \Drupal\commerce\EventBase $event
   *   A Commerce Order event.
   */
  public function orderEventResponse(EventBase $event) : void {
    $order = $event->getOrder();
    if ($order->getEmail() !== NULL
      && $order->hasItems()
      && $order->getState()->getId() === 'draft'
    ) {
      $this->eventResponse($order, $event);
    }
  }

  /**
   * Respond to order placed by deleting the cart and creating an order.
   */
  public function orderPlace(Event $event): void {
    $order = $event->getEntity();
    $session = \Drupal::service('request_stack')->getCurrentRequest()->getSession();
    $data = [
      'order_id' => $order->id(),
      'email' => $order->getEmail(),
      'event' => 'orderPlace',
      'order_item_id' => NULL,
      'campaign_id' => $session->get('mc_cid', NULL),
      'landing_site' => $session->get('mc_landing_site', NULL),
    ];

    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * Deletes a cart or order from Mailchimp.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   */
  public function orderDeleteResponse(OrderEvent $event) {
    $order = $event->getOrder();
    $data = [
      'order_id' => $order->id(),
      'event' => 'orderDelete',
    ];
    $this->deleteQueueItems($this::QUEUE_NAME, 'order_id', $data['order_id']);
    $this->createQueueItem($this::QUEUE_NAME, $data);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'commerce_order.place.post_transition' => 'orderPlace',
      OrderEvents::ORDER_ASSIGN => 'orderEventResponse',
      OrderEvents::ORDER_UPDATE => 'orderEventResponse',
      OrderEvents::ORDER_CREATE => 'orderEventResponse',
      OrderEvents::ORDER_DELETE => 'orderDeleteResponse',
      CartEvents::CART_EMPTY => 'cartEventResponse',
    ];
  }

}
