<?php

namespace Drupal\mailchimp_ecommerce_async\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Base functionality for Mailchimp E-commerce event subscribers.
 */
abstract class BaseEventSubscriber implements EventSubscriberInterface {

  /**
   * Check if duplicates exists, and if not, create the new queue item.
   *
   * @param string $queue_name
   * @param array $data
   * @return void
   */
  public function createQueueItem(string $queue_name, array $data): void {
    $queue = \Drupal::queue($queue_name);
    $results = $this->getDuplicateQueueItems($queue_name, $data);
    if ($results === []) {
      $queue->createItem($data);
    }
  }

  /**
   * Delete all queue items that have a matching key/value pair in their data.
   *
   * @param string $queue_name
   * @param string $key
   * @param string $value
   * @return void
   */
  public function deleteQueueItems(string $queue_name, string $key, string $value): void {
    $queue = \Drupal::queue($queue_name);
    $query = \Drupal::database()->select('queue', 'q');
    $query->condition('name', $queue_name);
    $query->fields('q', ['item_id', 'data']);
    $results = $query->execute()->fetchAll();
    foreach($results as $result) {
      $result_data = unserialize($result->data, ['allowed_classes' => FALSE]);
      if ($result_data[$key] == $value) {
        $queue->deleteItem($queue->claimItem($result->item_id));
      }
    }
  }

  /**
   * Returns a list of queue item ids with matching data array.
   *
   * @param $queue_name
   * @param $data
   * @return array
   */
  private function getDuplicateQueueItems(string $queue_name, array $data): array {
    $item_ids = [];
    $query = \Drupal::database()->select('queue', 'q');
    $query->condition('name', $queue_name);
    $query->fields('q', ['item_id', 'data']);
    $results = $query->execute()->fetchAll();
    foreach($results as $result) {
      $result_data = unserialize($result->data, ['allowed_classes' => FALSE]);
      if ($result_data == $data) {
        $item_ids[] = $result->item_id;
      }
    }
    return $item_ids;
  }

}
