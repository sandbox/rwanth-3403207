<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class OrderWorkflowMapForm extends ConfigFormBase {

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Module Handler Interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Entity Type Bundle Info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * MailchimpEcommerceAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory Interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager Interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The Field Manager Interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Interface.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The Entity Type Bundle Info.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager, ModuleHandlerInterface $module_handler, EntityTypeBundleInfo $entity_type_bundle_info) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_ecommerce_async.order_workflow_map'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_order_workflow_map';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['#tree'] = TRUE;
    $form['help'] = [
      '#markup' => t(
        '<p>Order notifications are triggered by a change in a contact’s order 
        status. After designing and enabling order notifications in your 
        Mailchimp account, you can trigger those emails by adding or updating an
        e-commerce order with <code>financial_status</code> or 
        <code>fulfillment_status</code> values.</p><p>The Mailchimp E-Commerce 
        module will listen to the workflow transitions selected below 
        and queue an order update when they occur. If no transition is selected
        for a Mailchimp state, it will be ignored.</p>'),
    ];
    $order_types = \Drupal::entityQuery('commerce_order_type')
      ->accessCheck('FALSE')
      ->execute();
    $form['order_types'] = [];

    foreach ($order_types as $type_id) {
      $type = OrderType::load($type_id);
      $workflow_id = $type->getWorkflowId();
      $workflow_manager = \Drupal::service('plugin.manager.workflow');
      $workflow = $workflow_manager->createInstance($workflow_id);
      $transitions = $workflow->getTransitions();
      $options = ['' => t('-- Select --')];
      foreach ($transitions as $t) {
        $options[$t->getId()] = $t->getLabel();
      }

      $form['order_types'][$type_id] = [
        '#type' => 'fieldset',
        '#title' => t('Mapping for order type %type using workflow %workflow',
          [
            '%type' => $type->get('label'),
            '%workflow' => $workflow->getLabel(),
          ]),
      ];
      $form['order_types'][$type_id]['paid'] = [
        '#type' => 'select',
        '#title' => t('Financial Status: Paid'),
        '#description' => t('Select the workflow transition event which will 
          transition orders of type %type to <code>paid</code>. This will trigger an 
          "order invoice" email in Mailchimp, notifying the customer that their 
          order is paid in full',
          ['%type' => $type_id]),
        '#options' => $options,
        '#default_value' => $this->config('mailchimp_ecommerce_async.order_workflow_map')
          ->get('order_types.' . $type_id . '.paid'),
      ];
      $form['order_types'][$type_id]['pending'] = [
        '#type' => 'select',
        '#title' => t('Financial Status: Pending'),
        '#description' => t('Select the workflow transition event which will 
          transition orders of type %type to <code>pending</code>. This will 
          trigger an "order confirmation" email in Mailchimp, notifying a customer if 
          their order is unpaid or partially paid',
          ['%type' => $type_id]),
        '#options' => $options,
        '#default_value' => $this->config('mailchimp_ecommerce_async.order_workflow_map')
          ->get('order_types.' . $type_id . '.pending'),
      ];
      $form['order_types'][$type_id]['refunded'] = [
        '#type' => 'select',
        '#title' => t('Financial Status: Refunded'),
        '#description' => t('Select the workflow transition event which will 
          transition orders of type %type to <code>refunded</code>. This will trigger a
          "refund confirmation" email in Mailchimp, notifying a customer that 
          their refund has been processed.',
          ['%type' => $type_id]),
        '#options' => $options,
        '#default_value' => $this->config('mailchimp_ecommerce_async.order_workflow_map')
          ->get('order_types.' . $type_id . '.refunded'),
      ];
      $form['order_types'][$type_id]['cancelled'] = [
        '#type' => 'select',
        '#title' => t('Financial Status: Cancelled'),
        '#description' => t('Select the workflow transition event which will 
          transition orders of type %type to <code>cancelled</code>. This will trigger a
          "cancellation confirmation" email in Mailchimp, notifying a customer 
          that their order has been cancelled.',
          ['%type' => $type_id]),
        '#options' => $options,
        '#default_value' => $this->config('mailchimp_ecommerce_async.order_workflow_map')
          ->get('order_types.' . $type_id . '.cancelled'),
      ];
      $form['order_types'][$type_id]['shipped'] = [
        '#type' => 'select',
        '#title' => t('Fulfillment Status: Shipped'),
        '#description' => t('Select the workflow transition event which will 
          transition orders of type %type to <code>shipped</code>. This will trigger a 
          "shipping confirmation," notifying a customer that their order has 
          been shipped.',
          ['%type' => $type_id]),
        '#options' => $options,
        '#default_value' => $this->config('mailchimp_ecommerce_async.order_workflow_map')
          ->get('order_types.' . $type_id . '.shipped'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('mailchimp_ecommerce_async.order_workflow_map');
    $values = $form_state->cleanValues()->getValues();
    $config
      ->set('order_types', $values['order_types'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
