<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Form\FormStateInterface;
use MailchimpMarketing\ApiException;

/**
 *
 */
class StoreUpdateForm extends StoreFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_store_update';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $store_id = \Drupal::configFactory()->get('mailchimp_ecommerce_async.settings')->get('store_id');
    try {
      $store = $this->storeHandler->getStore($store_id);
    }
    catch (ApiException $e) {
      $form['no_stores'] = [
        '#markup' => t('There are no stores in mailchimp to delete.'),
      ];
      return $form;
    }

    if (empty($store)) {
      $form['no_stores'] = [
        '#markup' => t('There are no stores in mailchimp to delete.'),
      ];
      return $form;
    }

    $form['details'] = [
      '#markup' => t('Connected to store: %store', ['%store' => $store->name]),
    ];

    $form['id'] = [
      '#type' => 'hidden',
      '#default_value' => $store_id,
    ];

    $form['#tree'] = TRUE;
    $form['#redirect'] = '/admin/config/services/mailchimp_ecommerce_async';

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Store Name',
      '#required' => TRUE,
      '#description' => t('The name of your store as it should appear in your Mailchimp account.'),
      '#default_value' => $store->name,
    ];

    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => 'Phone number',
      '#default_value' => $store->phone,
    ];

    $form['currency_code'] = [
      '#type' => 'textfield',
      '#title' => 'Currency Code',
      '#required' => TRUE,
      '#default_value' => $store->currency_code,
    ];

    $form['money_format'] = [
      '#type' => 'textfield',
      '#title' => 'Money format',
      '#description' => t('The currency format for the store. For example: $, £, etc.'),

      '#default_value' => $store->money_format,
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => 'Domain',
      '#default_value' => $store->domain,
    ];

    $form['platform'] = [
      '#type' => 'textfield',
      '#title' => 'Platform',
      '#default_value' => $store->platform,
    ];

    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => 'Email Address',
      '#default_value' => $store->email_address,
    ];

    $form['primary_locale'] = [
      '#type' => 'textfield',
      '#title' => 'Primary locale',
      '#default_value' => $store->primary_locale,
    ];

    $form['timezone'] = [
      '#type' => 'textfield',
      '#title' => 'Timezone',
      '#default_value' => $store->timezone,
    ];

    $address = $store->address;

    $form['address'] = [
      '#type' => 'fieldset',
      '#title' => 'Address',
      'address1' => [
        '#type' => 'textfield',
        '#title' => 'Address 1',
        '#default_value' => $address->address1,
      ],
      'address2' => [
        '#type' => 'textfield',
        '#title' => 'Address 2',
        '#default_value' => $address->address2,
      ],
      'city' => [
        '#type' => 'textfield',
        '#title' => 'City',
        '#default_value' => $address->city,
      ],
      'province' => [
        '#type' => 'textfield',
        '#title' => 'Province',
        '#default_value' => $address->province,
      ],
      'province_code' => [
        '#type' => 'textfield',
        '#title' => 'Province code',
        '#default_value' => $address->province_code,
      ],
      'postal_code' => [
        '#type' => 'textfield',
        '#title' => 'Postal code',
        '#default_value' => $address->postal_code,
      ],
      'country' => [
        '#type' => 'textfield',
        '#title' => 'Country',
        '#default_value' => $address->country,
      ],
      'country_code' => [
        '#type' => 'textfield',
        '#title' => 'Country code',
        '#default_value' => $address->country_code,
      ],
      'longitude' => [
        '#type' => 'number',
        '#title' => 'Longitude',
        '#default_value' => $address->longitude !== 0 ? $address->longitude : NULL,
      ],
      'latitude' => [
        '#type' => 'number',
        '#title' => 'Latitude',
        '#default_value' => $address->latitude !== 0 ? $address->latitude : NULL,
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update store in Mailchimp'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $body = $form_state->cleanValues()->getValues();
    $id = $body['id'];
    unset($body['actions'], $body['details'], $body['id']);
    $body['address'] = array_filter($body['address']);
    $body = array_filter($body);

    try {
      $this->storeHandler
        ->updateStore($id, $body);
      $this->messenger()
        ->addStatus($this->t('The store has been updated in Mailchimp.'));
    }
    catch (\Exception $e) {
      $this->logger('mailchimp_store_update')
        ->error('Could not update store in Mailchimp: ' . $e->getMessage());
    }
  }

}
