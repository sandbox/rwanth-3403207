<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class StoreCreateForm extends StoreFormBase {

  /**
   * @var string|null $storeId
   *   The commerce store ID to use as default field values for this form.
   */
  protected $storeId;

  /**
   * @var StoreInterface|null $store
   */
  protected $store;

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_store_create';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $commerce_stores = \Drupal::entityQuery('commerce_store')
      ->accessCheck('FALSE')
      ->execute();
    if (empty($commerce_stores)) {
      $form['no_store'] = [
        '#type' => 'processed_text',
        '#text' => 'There are no Stores configured in Drupal Commerce. Please
        finish configuring a store before attempting to initialize your 
        Mailchimp E-commerce connection.',
      ];
      return $form;
    }

    $form['#tree'] = TRUE;
    $form['#redirect'] = '/admin/config/services/mailchimp_ecommerce_async';

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => 'Store ID',
      '#required' => TRUE,
      '#default_value' => uniqid('', TRUE),
      '#description' => t('The unique identifier for the store.'),
    ];

    $mailchimp_lists = $this->storeHandler->getLists()?->lists;
    $list_options = ['' => '-- Select --'];

    foreach ($mailchimp_lists as $list) {
      $list_options[$list->id] = $list->name;
    }

    $form['list_id'] = [
      '#type' => 'select',
      '#title' => t('Store Audience'),
      '#required' => TRUE,
      '#options' => $list_options,
      '#description' => t('Once created, the audience cannot be changed for a given store.'),
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Store Name',
      '#required' => TRUE,
      '#description' => t('The name of your store as you want it to appear in your Mailchimp account.'),
    ];

    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => 'Phone number',
    ];

    $stores = ['' => 'None'];
    foreach ($commerce_stores as $store_id) {
      $stores[$store_id] = \Drupal::entityTypeManager()
        ->getStorage('commerce_store')
        ?->load($store_id)
      ->label();
    }
    $form['selected_store'] = [
      '#type' => 'radios',
      '#title' => 'Create using default values from Commerce Store. See /admin/commerce/config/stores',
      '#description' => t('Using defaults from a store is recommended'),
      '#options' => $stores,
      '#default_value' => '',
      '#ajax' => [
        'callback' => '::ajaxStoreSelect',
        'wrapper' => 'mc-create-store-fields',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['store_fields'] = [
      '#type' => 'fieldset',
      '#id' => 'mc-create-store-fields',
    ];

    $form['store_fields']['currency_code'] = [
      '#type' => 'textfield',
      '#title' => 'Currency Code',
      '#required' => TRUE,
    ];

    $form['store_fields']['money_format'] = [
      '#type' => 'textfield',
      '#title' => 'Money format',
      '#description' => t('The currency format for the store. For example: $, £, etc.'),
    ];

    $form['store_fields']['domain'] = [
      '#type' => 'textfield',
      '#title' => 'Domain',
      '#default_value' => \Drupal::request()->getHost(),
    ];

    $form['store_fields']['platform'] = [
      '#type' => 'textfield',
      '#title' => 'Platform',
      '#default_value' => 'Drupal Commerce',
    ];

    $form['store_fields']['email_address'] = [
      '#type' => 'textfield',
      '#title' => 'Email Address',
    ];

    $form['store_fields']['primary_locale'] = [
      '#type' => 'textfield',
      '#title' => 'Primary locale',
      '#default_value' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ];

    $form['store_fields']['timezone'] = [
      '#type' => 'textfield',
      '#title' => 'Timezone',
    ];

    $form['store_fields']['address'] = [
      '#type' => 'fieldset',
      '#title' => 'Address',

      'address1' => [
        '#type' => 'textfield',
        '#title' => 'Address 1',
      ],
      'address2' => [
        '#type' => 'textfield',
        '#title' => 'Address 2',
      ],
      'city' => [
        '#type' => 'textfield',
        '#title' => 'City',
      ],
      'province' => [
        '#type' => 'textfield',
        '#title' => 'Province',
      ],
      'province_code' => [
        '#type' => 'textfield',
        '#title' => 'Province code',
      ],
      'postal_code' => [
        '#type' => 'textfield',
        '#title' => 'Postal code',
      ],
      'country' => [
        '#type' => 'textfield',
        '#title' => 'Country',
      ],
      'country_code' => [
        '#type' => 'textfield',
        '#title' => 'Country code',
      ],
      'longitude' => [
        '#type' => 'number',
        '#title' => 'Longitude',
      ],
      'latitude' => [
        '#type' => 'number',
        '#title' => 'Latitude',
      ],
    ];


    $trigger = $form_state->getTriggeringElement();

    if ($trigger && $trigger['#name'] === 'selected_store') {
      $this->storeId = $form_state->getTriggeringElement()['#value'];
      $this->store = !empty($this->storeId) ? \Drupal::entityTypeManager()->getStorage('commerce_store')?->load($this->storeId) : NULL;
      if ($this->store instanceof StoreInterface) {
        $user_input = $form_state->getUserInput();
        $user_input['store_fields']['currency_code']= $this->store->getDefaultCurrencyCode();
        $user_input['store_fields']['money_format']= $this->store->getDefaultCurrency()->getSymbol();
        $user_input['store_fields']['email_address'] = $this->store->getEmail();
        $user_input['store_fields']['timezone'] = $this->store->getTimezone();
        $address = $this->store->getAddress();
        $user_input['store_fields']['address']['address1'] = $address->getAddressLine1();
        $user_input['store_fields']['address']['address2'] = $address->getAddressLine2();
        $user_input['store_fields']['address']['city'] = $address->getLocality();
        $user_input['store_fields']['address']['province'] = $address->getAdministrativeArea();
        $user_input['store_fields']['address']['postal_code'] = $address->getPostalCode();
        $user_input['store_fields']['address']['country_code'] = $address->getCountryCode();
        $form_state->setUserInput($user_input);
      }
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create store in Mailchimp'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $body = $form_state->cleanValues()->getValues();
    $body += $body['store_fields'];
    unset($body['actions'], $body['use_store_defaults'], $body['store_fields']);
    $body['address'] = array_filter($body['address']);
    $body = array_filter($body);
    try {
      $this->storeHandler
        ->addStore($body);
      $this->messenger()
        ->addStatus($this->t('The store has been created in Mailchimp.'));
    }
    catch (\Exception $e) {
      $this->logger('mailchimp_store_create')
        ->error('Could not create store in Mailchimp: ' . $e->getMessage());
    }
  }

  /**
   * An Ajax callback.
   */
  public function ajaxStoreSelect(array &$form, FormStateInterface $form_state) {
    return $form['store_fields'];
  }

}
