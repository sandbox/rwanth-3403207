<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ProductPropertyMapForm extends ConfigFormBase {

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Module Handler Interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Entity Type Bundle Info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * MailchimpEcommerceAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory Interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager Interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The Field Manager Interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Interface.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The Entity Type Bundle Info.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager, ModuleHandlerInterface $module_handler, EntityTypeBundleInfo $entity_type_bundle_info) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_ecommerce_async.product_property_map'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_product_property_map';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['#tree'] = TRUE;
    $form['help'] = [
      '#markup' => t(
        '<p>Define property mappings for each Product Type and Variation Type. 
        In a product type, you can choose to select a field value from the 
        default variation instead of from the product.</p>'),
    ];

    // @TODO Figure out how to support handle, vendor, images.
    $mc_product_fields = [
      'description',
      'image_url',
    ];
    $this->buildSubForm($form, 'commerce_product', $mc_product_fields);

    // @TODO Figure out how to support backorders.
    $mc_variant_fields = array_merge($mc_product_fields, [
      'inventory_quantity',
    ]);
    $this->buildSubForm($form, 'commerce_product_variation', $mc_variant_fields);

    return $form;
  }

  /**
   *
   */
  public function buildSubForm(array &$form, string $entity_type_id, array $mailchimp_fields) {
    $product_types = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    $form[$entity_type_id] = [
      '#type' => 'fieldset',
      '#title' => t('Manage %type field mappings', [
        '%type' => $entity_type_id,
      ]),
    ];

    foreach ($product_types as $bundle => $label) {
      $form[$entity_type_id][$bundle] = [
        '#type' => 'fieldset',
        '#title' => t('%type: %label', [
          '%type' => $entity_type_id,
          '%label' => $label['label'],
        ]),
      ];

      foreach ($mailchimp_fields as $mailchimp_field) {
        if ($entity_type_id === 'commerce_product') {
          $concat_id = $entity_type_id . '_' . $bundle . '_' . $mailchimp_field . '_use_variation';
          $options = $this->generateOptions($entity_type_id, $bundle, $mailchimp_field);
          $form[$entity_type_id][$bundle][$mailchimp_field]['drupal_field'] = [
            '#type' => 'select',
            '#title' => t('Product %field', ['%field' => $mailchimp_field]),
            '#multiple' => FALSE,
            '#description' => t('Please choose the %field field for your products.', ['%field' => $mailchimp_field]),
            '#options' => $options,
            '#states' => [
              'invisible' => [
                ':input[id="' . $concat_id . '"]' => ['checked' => TRUE],
              ],
            ],
            '#default_value' => $this->config('mailchimp_ecommerce_async.product_property_map')
              ->get($entity_type_id . '.' . $bundle . '.' . $mailchimp_field . '.drupal_field'),
          ];
          $form[$entity_type_id][$bundle][$mailchimp_field]['use_variation'] = [
            '#type' => 'checkbox',
            '#title' => t('Replace product %field (above) with a value from the default product variation', ['%field' => $mailchimp_field]),
            '#attributes' => [
              'id' => $concat_id,
            ],
            '#default_value' => $this->config('mailchimp_ecommerce_async.product_property_map')
              ->get($entity_type_id . '.' . $bundle . '.' . $mailchimp_field . '.use_variation'),
          ];
        }

        if ($entity_type_id === 'commerce_product_variation') {
          $options = $this->generateOptions($entity_type_id, $bundle, $mailchimp_field);
          $form[$entity_type_id][$bundle][$mailchimp_field] = [
            '#type' => 'select',
            '#title' => t('Product %field', ['%field' => $mailchimp_field]),
            '#multiple' => FALSE,
            '#description' => t('Please choose the %field field for your products.', ['%field' => $mailchimp_field]),
            '#options' => $options,
            '#default_value' => $this->config('mailchimp_ecommerce_async.product_property_map')
              ->get($entity_type_id . '.' . $bundle . '.' . $mailchimp_field),
          ];
        }

      }
    }
  }

  /**
   *
   */
  public function generateOptions($entity_type_id, $bundle, $mailchimp_field) {
    $options = ['' => t('-- Select --')];
    $fields = $this->fieldManager->getFieldDefinitions($entity_type_id, $bundle);

    foreach ($fields as $field_name => $field_properties) {
      switch ($mailchimp_field) {
        case 'description':
          if (str_starts_with($field_properties->getType(), 'text')) {
            $options[$field_name] = $field_name;
          }
          break;

        case 'image_url':
          if ($field_properties->getType() === 'image' || $field_properties->getSetting('target_type') === 'media') {
            $options[$field_name] = $field_name;
          }
          break;

        case 'images':
          if ($field_properties->getType() === 'image'
            && $field_properties->isList()) {
            $options[$field_name] = $field_name;
          }
          break;

        case 'inventory_quantity':
          if (in_array($field_properties->getType(), [
            'integer',
            'float',
            'decimal',
          ])) {
            $options[$field_name] = $field_name;
            $options['non_inventory'] = 'Do not use inventory';
          }
          break;

        default:
          $options[$field_name] = $field_name;
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('mailchimp_ecommerce_async.product_property_map');
    $values = $form_state->cleanValues()->getValues();
    $config
      ->set('commerce_product', $values['commerce_product'])
      ->set('commerce_product_variation', $values['commerce_product_variation'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
