<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Form\FormStateInterface;
use MailchimpMarketing\ApiException;

/**
 *
 */
class StoreDeleteForm extends StoreFormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_store_delete';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $stores = [];
    try {
      $response = $this->storeHandler->getStores();
      $stores = $response->stores;
    }
    catch (ApiException $e) {
    }

    if ($stores !== []) {
      $store_select = [];
      foreach ($stores as $store) {
        $store_select[$store->id] = $store->name;
      }
      $form['id'] = [
        '#type' => 'radios',
        '#title' => 'Select a store',
        '#options' => $store_select,
      ];
    }

    else {
      $form['no_stores'] = [
        '#markup' => t('There are no stores in mailchimp to delete.'),
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete store'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getValue('id');
    try {
      $this->storeHandler
        ->deleteStore($id);
      $this->messenger()
        ->addStatus(t('The store has been deleted in Mailchimp.'));
    }
    catch (\Exception $e) {
      $this->logger('mailchimp_store_delete')
        ->error('Could not delete store in Mailchimp: ' . $e->getMessage());
    }
  }

}
