<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailchimp_ecommerce_async\Contracts\OrderHandlerInterface;
use Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface;

/**
 *
 */
class OrderSyncForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_order_sync';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['timespan'] = [
      '#type' => 'textfield',
      '#title' => t('Time span'),
      '#default_value' => 6,
      '#field_suffix' => 'months',
      '#description' => 'Mailchimp recommends syncing the past 6 months of order data. Leave blank to sync all orders.',
      '#size' => 3,
      '#maxlength' => 3,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync with Mailchimp'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => t('Syncing orders to Mailchimp'),
      'operations' => [],
    ];

    if (!empty($form_state->getValue('timespan'))) {
      $date_string = $form_state->getValue('timespan') . ' months ago';
      $request = strtotime($date_string);

      $query = \Drupal::entityQuery('commerce_order')
        ->condition('placed', $request, '>=')
        ->accessCheck('FALSE');
    }
    else {
      $query = \Drupal::entityQuery('commerce_order')
        ->accessCheck('FALSE');
    }
    $result = $query->execute();

    if (!empty($result)) {
      $order_ids = array_keys($result);

      $batch['operations'][] = [
        '\Drupal\mailchimp_ecommerce_async\Form\OrderSyncForm::syncOrders',
        [$order_ids],
      ];
    }

    batch_set($batch);
  }

  /**
   * Batch processor for Order sync.
   *
   * @param array $order_ids
   *   IDs of orders to sync.
   * @param array $context
   *   Batch process context; stores progress data.
   */
  public static function syncOrders($order_ids, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['total'] = count($order_ids);
      $context['results']['order_ids'] = $order_ids;
    }

    $config = \Drupal::config('mailchimp_ecommerce_async.settings');
    $batch_limit = $config?->get('batch_limit') ?? 100;

    $batch = array_slice($context['results']['order_ids'], $context['sandbox']['progress'], $batch_limit);

    $order_handler = \Drupal::service('mailchimp_ecommerce_async.order_handler');
    $store_handler = \Drupal::service('mailchimp_ecommerce_async.store_handler');
    $store_handler->enableSyncing();

    foreach ($batch as $order_id) {
      $order_handler->syncOrder($order_id);

      $context['sandbox']['progress']++;

      $context['message'] = t('Sent @count of @total orders to Mailchimp', [
        '@count' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['total'],
      ]);

      $context['finished'] = ($context['sandbox']['progress'] / $context['sandbox']['total']);
    }

    $store_handler->disableSyncing();
  }

}
