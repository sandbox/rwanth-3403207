<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * The Store Handler Interface.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface
   */
  protected $storeHandler;

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Module Handler Interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MailchimpEcommerceAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory Interface.
   * @param \Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface $store_handler
   *   The Store Handler Interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The Field Manager Interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StoreHandlerInterface $store_handler, EntityFieldManagerInterface $field_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);

    $this->storeHandler = $store_handler;
    $this->fieldManager = $field_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('mailchimp_ecommerce_async.store_handler'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_ecommerce_async.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_store_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('mailchimp_ecommerce_async.settings');
    $store_id = $config->get('store_id');
    $api_key = $config->get('api_key');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => 'Mailchimp API Key',
      '#required' => TRUE,
      '#default_value' => $api_key,
      '#ajax' => [
        'callback' => '::ajaxApiKeyChange',
        'wrapper' => 'select-mc-store',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    if (!empty($api_key)) {
      $stores = [];

      try {
        $response = $this->storeHandler->getStores();
        $stores = $response?->stores;
      }
      catch (RequestException $e) {
        // @TODO log something at least.
      }

      if (!empty($stores)) {
        $store_select = ['' => '-- Select --'];
        foreach ($stores as $store) {
          $store_select[$store->id] = $store->name;
        }
        $form['store_id'] = [
          '#type' => 'select',
          '#title' => 'Select a Mailchimp Store to connect to Drupal',
          '#options' => $store_select,
          '#default_value' => $store_id,
          '#prefix' => '<div id="select-mc-store-id">',
          '#suffix' => '</div>',
        ];
      }

      $form['double_opt_in'] = [
        '#type' => 'checkbox',
        '#title' => 'Require subscribers double opt-in',
        '#default_value' => $config->get('double_opt_in'),
      ];

      $form['batch_limit'] = [
        '#type' => 'select',
        '#options' => [
          '1' => '1',
          '10' => '10',
          '25' => '25',
          '50' => '50',
          '75' => '75',
          '100' => '100',
          '250' => '250',
          '500' => '500',
          '750' => '750',
          '1000' => '1000',
          '2500' => '2500',
          '5000' => '5000',
          '7500' => '7500',
          '10000' => '10000',
        ],
        '#title' => t('Batch limit'),
        '#description' => t('Maximum number of entities to process at once. Mailchimp suggest keeping this at 5000 or below.'),
        '#default_value' => $config->get('batch_limit'),
      ];

      $form['store'] = [
        '#type' => 'fieldset',
        '#title' => 'Store operations',
        'add' => [
          '#markup' =>
          '<p>' .
          Link::fromTextAndUrl(t('Create a new store in Mailchimp'),
            Url::fromRoute('mailchimp_ecommerce_async.store_create'))
            ->toString()
          . '</p>',
        ],
        'update' => [
          '#markup' =>
          '<p>' .
          Link::fromTextAndUrl(t('Update Mailchimp connected store'),
            Url::fromRoute('mailchimp_ecommerce_async.store_update'))
            ->toString()
          . '</p>',
        ],
        'delete' => [
          '#markup' =>
          '<p>' .
          Link::fromTextAndUrl(t('Delete a store in Mailchimp'),
            Url::fromRoute('mailchimp_ecommerce_async.store_delete'))
            ->toString()
          . '</p>',
        ],
      ];

      if (!empty($config->get('store_id'))) {
        $form['product-sync'] = [
          '#type' => 'fieldset',
          '#title' => t('Product sync'),
          '#collapsible' => FALSE,
        ];
        $form['product-sync']['products'] = [
          '#markup' => Link::fromTextAndUrl(t('Sync existing products to Mailchimp'), Url::fromRoute('mailchimp_ecommerce_async.product_sync'))
            ->toString(),
        ];
        $form['order-sync'] = [
          '#type' => 'fieldset',
          '#title' => t('Order sync'),
          '#collapsible' => FALSE,
        ];
        $form['order-sync']['orders'] = [
          '#markup' => Link::fromTextAndUrl(t('Sync existing orders to Mailchimp'), Url::fromRoute('mailchimp_ecommerce_async.order_sync'))
            ->toString(),
        ];
        $form['promo-sync'] = [
          '#type' => 'fieldset',
          '#title' => t('Promo sync'),
          '#collapsible' => FALSE,
        ];
        $form['promo-sync']['promo'] = [
          '#markup' => Link::fromTextAndUrl(t('Sync existing promotions and coupons to Mailchimp'), Url::fromRoute('mailchimp_ecommerce_async.promo_sync'))
            ->toString(),
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    if (empty($api_key)) {
      $form_state->setErrorByName('api_key', 'You must provide an API Key.');
    }
    elseif (!$this->storeHandler->isApiKeyValid($api_key)) {
      $form_state->setErrorByName('api_key', 'The API Key is not valid.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('mailchimp_ecommerce_async.settings');
    $values = $form_state->cleanValues()->getValues();
    $config->setData($values)->save();

    if ($values['store_id']) {
      try {
        $response = $this->storeHandler->getStore($values['store_id']);
        $config->set('list_id', $response?->list_id)->save();
      }
      catch (RequestException $e) {
        // @TODO log something at least.
      }
    }

    parent::submitForm($form, $form_state);
  }


  public function ajaxApiKeyChange(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $api_key = $form_state->getValue('api_key');
    if (!empty($api_key)) {
      try {
        if ($this->storeHandler->isApiKeyValid($api_key)) {
          $this->storeHandler->setNewApiObject($api_key);
          $store_response = $this->storeHandler->getStores();
          $stores = $store_response?->stores;
          if (!empty($stores)) {
            $store_select = ['' => '-- Select --'];
            foreach ($stores as $store) {
              $store_select[$store->id] = $store->name;
            }
            $form['store_id']['#options'] = $store_select;
            $ajax_response->addCommand(new ReplaceCommand('#select-mc-store-id', $form['store_id']));
          }
        }
        else {
          $ajax_response->addCommand(new MessageCommand('You must enter a valid API key.', NULL, ['type' => 'error']));
        }
      }
      catch (RequestException $e) {
        // @TODO log something at least.
        $ajax_response->addCommand(new MessageCommand('Could not fetch stores from Mailchimp',
          NULL,
          ['type' => 'error']
        ));
      }
    }
    else {
      $ajax_response->addCommand(new MessageCommand('You must enter a valid API key.', NULL, ['type' => 'error']));
    }

    return $ajax_response;
  }
}
