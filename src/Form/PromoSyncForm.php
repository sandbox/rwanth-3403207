<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailchimp_ecommerce_async\Contracts\ProductHandlerInterface;
use Drupal\mailchimp_ecommerce_async\Contracts\PromoHandlerInterface;

/**
 * Send all Promotions and Coupons to Mailchimp as Promo Rules and Promo Codes.
 */
class PromoSyncForm extends FormBase{
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_promo_sync';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync all Promotions with Mailchimp'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => t('Adding promotions to Mailchimp'),
      'operations' => [],
    ];
    $query = \Drupal::entityQuery('commerce_promotion')
      ->accessCheck('FALSE');
    $result = $query->execute();

    if (!empty($result)) {
      $product_ids = array_keys($result);

      $batch['operations'][] = [
        '\Drupal\mailchimp_ecommerce_async\Form\PromoSyncForm::syncPromos',
        [$product_ids],
      ];
    }

    batch_set($batch);
  }

  public static function syncPromos($promotion_ids, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['total'] = count($promotion_ids);
      $context['results']['promotion_ids'] = $promotion_ids;
    }

    $config = \Drupal::config('mailchimp_ecommerce_async.settings');
    $batch_limit = $config->get('batch_limit') ?? 100;

    $batch = array_slice($context['results']['promotion_ids'], $context['sandbox']['progress'], $batch_limit);
    $promo_handler = \Drupal::service('mailchimp_ecommerce_async.promo_handler');

    foreach ($batch as $promotion_id) {
      $promo_handler->syncPromoRule($promotion_id);
      $promo = \Drupal::entityTypeManager()->getStorage('commerce_promotion')
        ->load($promotion_id);
      $coupon_ids = $promo->getCouponIds();
      foreach ($coupon_ids as $coupon_id) {
        $promo_handler->syncPromoCode($promotion_id, $coupon_id);
      }

      $context['sandbox']['progress']++;

      $context['message'] = t('Sent @count of @total promotions to Mailchimp', [
        '@count' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['total'],
      ]);

      $context['finished'] = ($context['sandbox']['progress'] / $context['sandbox']['total']);
    }
  }
}
