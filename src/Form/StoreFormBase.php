<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Initializes an api handler for forms that manipulate Mailchimp stores.
 */
abstract class StoreFormBase extends FormBase {
  /**
   * The API Handler Interface.
   *
   * @var \Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface
   */
  protected $storeHandler;

  /**
   * Construct delete store form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\mailchimp_ecommerce_async\Contracts\StoreHandlerInterface $store_handler
   *   The API handler for stores.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StoreHandlerInterface $store_handler) {
    $this->configFactory = $config_factory;
    $this->storeHandler = $store_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('mailchimp_ecommerce_async.store_handler'),
    );
  }

}
