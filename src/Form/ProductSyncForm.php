<?php

namespace Drupal\mailchimp_ecommerce_async\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailchimp_ecommerce_async\Contracts\ProductHandlerInterface;

/**
 *
 */
class ProductSyncForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_async_product_sync';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['sync_products'] = [
      '#type' => 'checkbox',
      '#title' => t('Sync Products'),
      '#description' => t('Add or update all products to Mailchimp.'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync with Mailchimp'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if (!empty($form_state->getValue('sync_products'))) {
      $batch = [
        'title' => t('Adding products to Mailchimp'),
        'operations' => [],
      ];
      $query = \Drupal::entityQuery('commerce_product')
        ->accessCheck('FALSE');
      $result = $query->execute();

      if (!empty($result)) {
        $product_ids = array_keys($result);

        $batch['operations'][] = [
          '\Drupal\mailchimp_ecommerce_async\Form\ProductSyncForm::syncProducts',
          [$product_ids],
        ];
      }

      batch_set($batch);
    }

    if (!empty($form_state->getValue('delete_products'))) {
      $batch = [
        'title' => t('Deleting products from Mailchimp'),
        'operations' => [],
      ];
      $query = \Drupal::entityQuery('commerce_product')
        ->accessCheck('FALSE');
      $result = $query->execute();

      if (!empty($result)) {
        $product_ids = array_keys($result);

        $batch['operations'][] = [
          '\Drupal\mailchimp_ecommerce_async\Form\ProductSyncForm::deleteProducts',
          [$product_ids],
        ];
      }

      batch_set($batch);
    }
  }

  /**
   *
   */
  public static function syncProducts($product_ids, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['total'] = count($product_ids);
      $context['results']['product_ids'] = $product_ids;
    }

    $config = \Drupal::config('mailchimp_ecommerce_async.settings');
    $batch_limit = $config->get('batch_limit') ?? 100;

    $batch = array_slice($context['results']['product_ids'], $context['sandbox']['progress'], $batch_limit);
    $product_handler = \Drupal::service('mailchimp_ecommerce_async.product_handler');

    foreach ($batch as $product_id) {
      $product_handler->syncProduct($product_id);

      $context['sandbox']['progress']++;

      $context['message'] = t('Sent @count of @total products to Mailchimp', [
        '@count' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['total'],
      ]);

      $context['finished'] = ($context['sandbox']['progress'] / $context['sandbox']['total']);
    }
  }

  /**
   * Delete products from a store.
   */
  public static function deleteProducts($product_ids, &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['total'] = count($product_ids);
      $context['results']['product_ids'] = $product_ids;
    }

    $config = \Drupal::config('mailchimp_ecommerce_async.settings');
    $batch_limit = $config->get('batch_limit') ?? 100;

    $batch = array_slice($context['results']['product_ids'], $context['sandbox']['progress'], $batch_limit);

    foreach ($batch as $product_id) {
      \Drupal::service('mailchimp_ecommerce_async.product_handler')
        ->deleteProduct($product_id);

      $context['sandbox']['progress']++;

      $context['message'] = t('Sent @count of @total products to Mailchimp', [
        '@count' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['total'],
      ]);

      $context['finished'] = ($context['sandbox']['progress'] / $context['sandbox']['total']);
    }
  }

}
