<?php

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\commerce_promotion\Entity\CouponInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Url;
use Drupal\mailchimp_ecommerce_async\Contracts\PromoHandlerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * {@inheritDoc}
 */
class PromoHandler extends ApiHandlerBase implements PromoHandlerInterface {

  /**
   * Sync a promo_rule between Drupal and Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The ID of the promo_rule to sync.
   */
  public function syncPromoRule(int|string $promo_rule_id): void {
    try {
      $mc_promo_rule = $this->getPromoRule($promo_rule_id);

      if (!is_null($mc_promo_rule)) {
        // Get promo_rule succeeded, we can now update it.
        $this->updatePromoRule($promo_rule_id);
      }
      else {
        // Get promo_rule did not find a promo_rule in Mailchimp, create it now.
        $this->addPromoRule($promo_rule_id);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync promo_rule failed for order %id.', ['%id' => $promo_rule_id])
      );
    }
    catch (EntityStorageException $entityStorageException) {
      $this->log($entityStorageException,
        $this->stringTranslation->translate('Sync promo_rule failed because order %id was deleted in Drupal before this queue item was processed.', ['%id' => $promo_rule_id])
      );
    }
  }

  /**
   * Sync a promo_code between Drupal and Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The ID of the promotion.
   * @param int|string $promo_code_id
   *   The ID of the coupon code.
   */
  public function syncPromoCode(int|string $promo_rule_id, int|string $promo_code_id): void {
    try {
      $mc_promo_code = $this->getPromoCode($promo_rule_id, $promo_code_id);

      if (!is_null($mc_promo_code)) {
        // Get promo_rule succeeded, we can now update it.
        $this->updatePromoCode($promo_rule_id, $promo_code_id);
      }
      else {
        // Get promo_rule did not find a promo_rule in Mailchimp, create it now.
        $this->addPromoCode($promo_rule_id, $promo_code_id);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync promo_code failed for order %id.', ['%id' => $promo_code_id])
      );
    }
    catch (EntityStorageException $entityStorageException) {
      $this->log($entityStorageException,
        $this->stringTranslation->translate('Sync promo_code failed because order %id was deleted in Drupal before this queue item was processed.', ['%id' => $promo_code_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPromoRules(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?\stdClass {
    try {
      return $this->api->ecommerce->listPromoRules($this->storeId, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPromoRule(int|string $promo_rule_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getPromoRule($this->storeId, $promo_rule_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPromoCodes(int|string $promo_rule_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?\stdClass {
    try {
      // Yes, the Rule ID and Store ID Order are switched for this function.
      return $this->api->ecommerce->getPromoCodes($promo_rule_id, $this->storeId, $fields, $exclude_fields, $count, $offset);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPromoCode(int|string $promo_rule_id, int|string $promo_code_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getPromoCode($this->storeId, $promo_rule_id, $promo_code_id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addPromoRule(int|string $promo_rule_id): ?\stdClass {
    $body = $this->buildPromoRuleBody($promo_rule_id);
    return $this->api->ecommerce->addPromoRules($this->storeId, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function addPromoCode(int|string $promo_rule_id, int|string $promo_code_id): ?\stdClass {
    $body = $this->buildPromoCodeBody($promo_code_id);
    return $this->api->ecommerce->addPromoCode($this->storeId, $promo_rule_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updatePromoRule(int|string $promo_rule_id): ?\stdClass {
    $body = $this->buildPromoRuleBody($promo_rule_id);
    return $this->api->ecommerce->updatePromoRule($this->storeId, $promo_rule_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updatePromoCode(int|string $promo_rule_id, int|string $promo_code_id): ?\stdClass {
    $body = $this->buildPromoCodeBody($promo_code_id);
    return $this->api->ecommerce->updatePromoCode($this->storeId, $promo_rule_id, $promo_code_id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function deletePromoRule(int|string $promo_rule_id): void {
    try {
      $this->api->ecommerce->deletePromoRule($this->storeId, $promo_rule_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete promo rule failed for promotion %id.', ['%id' => $promo_rule_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deletePromoCode(int|string $promo_rule_id, int|string $promo_code_id): void {
    try {
      $this->api->ecommerce->deletePromoCode($this->storeId, $promo_rule_id, $promo_code_id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete promo code failed for coupon %id.', ['%id' => $promo_code_id])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildPromoRuleBody(string $promo_rule_id, ?array $property_overrides = NULL): array {
    $promotion = $this->entityTypeManager
      ->getStorage('commerce_promotion')
      ->load($promo_rule_id);

    if (!($promotion instanceof PromotionInterface)) {
      throw new EntityStorageException('The promotion '. $promo_rule_id . ' does not exist.');
    }

    $body = [
      'id' => $promo_rule_id,
      'description' => $promotion->getDescription() ?? '',
      'starts_at' => $promotion->getStartDate()?->format('c') ?? '2000-01-01T00:00:00',
      'ends_at' => $promotion->getEndDate()?->format('c') ?? '2030-01-01T00:00:00',
      'enabled' => $promotion->isEnabled(),
      'created_at_foreign' => date('c', (int) $promotion->getCreatedTime()),
      'updated_at_foreign' => date('c'),
    ];

    $offer = $promotion->getOffer();
    $offer_config = $offer->getConfiguration();
    if (array_key_exists('amount', $offer_config)) {
      $body['type'] = 'fixed';
      $body['amount'] = $offer_config['amount']['number'];
    }
    elseif (array_key_exists('percentage', $offer_config)) {
      $body['type'] = 'percentage';
      $body['amount'] = $offer_config['percentage'];
    }
    // TODO figure out how to make this work with shipping promotions.
    else {
      $body['type'] = 'fixed';
      $body['amount'] = 0;
    }

    if ($offer->getEntityTypeId() === 'commerce_order') {
      $body['target'] = 'total';
    }
    elseif ($offer->getEntityTypeId() === 'commerce_order_item') {
      $body['target'] = 'per_item';
    }
    else {
      // @TODO This isn't necessarily true
      $body['target'] = 'shipping';
    }

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }

    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function buildPromoCodeBody(string $promo_code_id, ?string $redemption_url = NULL, ?array $property_overrides = NULL): array {
    $coupon = $this->entityTypeManager
      ->getStorage('commerce_promotion_coupon')
      ->load($promo_code_id);

    if (!($coupon instanceof CouponInterface)) {
      throw new EntityStorageException('The coupon '. $promo_code_id . ' does not exist.');
    }

    $body = [
      'id' => $promo_code_id,
      'promo_rule_id' => $coupon->getPromotionId(),
      'code' => $coupon->getCode(),
      'redemption_url' => $redemption_url,
      'enabled' => $coupon->isEnabled(),
      'created_at_foreign' => date('c', (int) $coupon->getCreatedTime()),
      'updated_at_foreign' => date('c'),
    ];

    if (empty($redemption_url)) {
      $body['redemption_url'] = Url::fromUri('internal:/', ['absolute' => TRUE])->toString();
    }

    if ($property_overrides) {
      $body = array_merge($body, $property_overrides);
    }

    return $body;
  }

}
