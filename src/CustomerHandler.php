<?php

namespace Drupal\mailchimp_ecommerce_async;

use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\mailchimp_ecommerce_async\Contracts\CustomerHandlerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * {@inheritDoc}
 */
class CustomerHandler extends ApiHandlerBase implements CustomerHandlerInterface {

  /**
   * {@inheritDoc}
   */
  public function syncCustomer(string $mail, bool $opt_in = FALSE): void {
    try {
      $response = $this->getCustomer($mail);

      if (is_null($response)) {
        $this->addCustomer($mail, $opt_in);
      }
      else {
        // Get customer succeeded, so the customer exists. We can now update it.
        $this->updateCustomer($mail, $opt_in);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync customer failed for customer %mail.', ['%mail' => $mail])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function syncCustomerDoubleOptIn(string $mail): void {
    try {
      $response = $this->getCustomer($mail);

      if (is_null($response)) {
        $response = $this->addCustomer($mail);
        $this->api->lists->setListMember(
          $this->config->get('list_id'),
          $response?->email_address,
          [
            'email_address' => $response?->email_address,
            'status_if_new' => 'pending',
          ]
        );
      }
      else {
        // Get customer succeeded, so the customer exists. We can now update it.
        $this->api->lists->setListMember(
          $this->config->get('list_id'),
          $response->email_address,
          [
            'email_address' => $response->email_address,
            'status_if_new' => 'pending',
            'status' => 'pending',
          ]
        );
        $this->updateCustomer($mail);
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }

      $this->log($e, $this->stringTranslation
        ->translate('Sync customer failed for customer %mail.', ['%mail' => $mail])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomers(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0, ?string $email_address = NULL): ?\stdClass {
    try {
      return $this->api->ecommerce->getAllStoreCustomers($this->storeId, $fields, $exclude_fields, $count, $offset, $email_address);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomer(string $mail, ?array $fields = NULL, ?array $exclude_fields = NULL): ?\stdClass {
    try {
      $id = $this->createIdFromMail($mail);
      return $this->api->ecommerce->getStoreCustomer($this->storeId, $id, $fields, $exclude_fields);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return NULL;
      }
      throw $e;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addCustomer(string $mail, bool $opt_in = FALSE): ?\stdClass {
    $body = $this->buildCustomerBody($mail, $opt_in);
    return $this->api->ecommerce->addStoreCustomer($this->storeId, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function updateCustomer(string $mail, bool $opt_in = FALSE): ?\stdClass {
    $body = $this->buildCustomerBody($mail, $opt_in);
    $id = $body['id'];
    return $this->api->ecommerce->updateStoreCustomer($this->storeId, $id, $body);
  }

  /**
   * {@inheritDoc}
   */
  public function deleteCustomer(string $mail): void {
    try {
      $id = $this->createIdFromMail($mail);
      $this->api->ecommerce->deleteStoreCustomer($this->storeId, $id);
    }
    catch (RequestException $e) {
      if ($e->getCode() === 429 || $e->getCode() >= 500) {
        throw new DelayedRequeueException(120, $e->getMessage(), $e->getCode(), $e);
      }
      $this->log($e, $this->stringTranslation
        ->translate('Delete customer failed for customer %mail.', ['%mail' => $mail])
      );
    }
  }

}
