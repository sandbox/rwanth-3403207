<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use stdClass;

/**
 * Covers Promo Rule and Promo Code endpoints in the Mailchimp Marketing API.
 *
 * Promo Rules help you create Promo Codes for your campaigns. Promo Rules
 * define generic information about Promo Codes like expiration time, start
 * time, amount of discount being offered etc. For a given Promo Rule you can
 * define if it's a percentage discount or a fixed amount and if it applies for
 * the Order as a whole or if it's per item or free shipping. You can then
 * create Promo Codes for this price rule. Promo Codes contain the actual code
 * that is applied at checkout along with some other information. Price Rules
 * have one-to-many relationship with Promo Codes.
 *
 * Promo Codes can be created for a given price rule. All the Promo Codes under
 * a price rule share the generic information defined for that rule like the
 * amount, type, expiration date etc. Promo Code defines the more specific
 * information about a Promo Code like the actual code, redemption_url,
 * usage_count, etc. that's unique to a code. Promo Code must be defined under a
 * Promo Rule.
 *
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-promo-rules/
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-promo-codes/
 */
interface PromoHandlerInterface {

  /**
   * Sync a Commerce Promotion with a Mailchimp Promo Rule.
   *
   * @param int|string $promo_rule_id
   *   The ID of the promo_rule to sync.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncPromoRule(int|string $promo_rule_id): void;

  /**
   * Sync a promo_code between Drupal and Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param int|string $promo_code_id
   *   The Commerce Promotion Coupon and Mailchimp Promo Code ID.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncPromoCode(int|string $promo_rule_id, int|string $promo_code_id): void;

  /**
   * Gets Promo Rules from Mailchimp.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getPromoRules(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a Promo Rule from Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getPromoRule(int|string $promo_rule_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Gets all Promo Codes from a Promo Rule in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getPromoCodes(int|string $promo_rule_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a Promo Code from a Promo Rule.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param int|string $promo_code_id
   *   The Commerce Promotion Coupon and Mailchimp Promo Code ID.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getPromoCode(int|string $promo_rule_id, int|string $promo_code_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Add a new Promo Rule in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addPromoRule(int|string $promo_rule_id): ?stdClass;

  /**
   * Add a new Promo Code to a Promo Rule in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param int|string $promo_code_id
   *   The Commerce Promotion Coupon and Mailchimp Promo Code ID.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addPromoCode(int|string $promo_rule_id, int|string $promo_code_id): ?stdClass;

  /**
   * Update a Promo Rule in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updatePromoRule(int|string $promo_rule_id): ?stdClass;

  /**
   * Update a Promo Code in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param int|string $promo_code_id
   *   The Commerce Promotion Coupon and Mailchimp Promo Code ID.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updatePromoCode(int|string $promo_rule_id, int|string $promo_code_id): ?stdClass;

  /**
   * Delete a Promo Rule in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deletePromoRule(int|string $promo_rule_id): void;

  /**
   * Delete a Promo Code in Mailchimp.
   *
   * @param int|string $promo_rule_id
   *   The Commerce Promotion and Mailchimp Promo Rule ID.
   * @param int|string $promo_code_id
   *   The Commerce Promotion Coupon and Mailchimp Promo Code ID.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deletePromoCode(int|string $promo_rule_id, int|string $promo_code_id): void;

  /**
   * @param string $promo_rule_id
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the Promo Rule. If
   *      Ecommerce platform does not support Promo Rule, use Promo Code id as
   *      Promo Rule id. Restricted to UTF-8 characters with max length 50.
   *   -  description: (string) Required. The description of a promotion
   *      restricted to UTF-8 characters with max length 255.
   *   -  amount: (number) Required. The amount of the Promo Code discount.
   *      If 'type' is 'fixed', the amount is treated as a monetary value.
   *      If 'type' is 'percentage', amount must be a decimal value between 0.0
   *      and 1.0, inclusive.
   *   -  type: (string) Required. Type of discount. For free shipping set type
   *      to fixed. Possible values: "fixed" or "percentage".
   *   -  target: (string) Required. The target that the discount applies to.
   *      Possible values: "per_item", "total", or "shipping".
   *   -  title: (string) The title that will show up in promotion campaign.
   *      Restricted to UTF-8 characters with max length of 100 bytes.
   *   -  starts_at: (string) The date and time when the promotion is in effect
   *      in ISO 8601 format.
   *   -  ends_at: (string) The date and time when the promotion ends. Must be
   *      after starts_at and in ISO 8601 format.
   *   -  enabled: (boolean) Whether the Promo Rule is currently enabled.
   *   -  created_at_foreign: (string) The date and time the promotion was
   *      created in ISO 8601 format.
   *   -  updated_at_foreign: (string) The date and time the promotion was
   *      updated in ISO 8601 format.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the promotion no longer exists.
   */
  public function buildPromoRuleBody(string $promo_rule_id, ?array $property_overrides = NULL): array;

  /**
   * @param string $promo_code_id
   * @param string|null $redemption_url
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the Promo Code.
   *      Restricted to UTF-8 characters with max length 50.
   *   -  code: (string) Required. The discount code. Restricted to UTF-8
   *      characters with max length 50.
   *   -  redemption_url: (string) Required. The url that should be used in the
   *      promotion campaign restricted to UTF-8 characters with max length
   *      2000.
   *   -  usage_count: (integer) Number of times Promo Code has been used.
   *   -  enabled: (boolean) Whether the Promo Code is currently enabled.
   *   -  created_at_foreign: (string) The date and time the promotion was
   *      created in ISO 8601 format.
   *   -  updated_at_foreign: (string) The date and time the promotion was
   *      updated in ISO 8601 format.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the coupon no longer exists.
   */
  public function buildPromoCodeBody(string $promo_code_id, ?string $redemption_url = NULL, ?array $property_overrides = NULL): array;

}
