<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\profile\Entity\ProfileInterface;
use MailchimpMarketing\ApiClient;

/**
 * Common functionality for all Mailchimp E-commerce API Handlers.
 *
 * Sets up API connection and translates Drupal entities into arrays that can
 * be sent as the body in a Mailchimp Marketing API call.
 */
interface ApiHandlerBaseInterface {

  /**
   * Confirm that Mailchimp Marketing library is installed.
   *
   * @return bool
   *   Whether the library is installed.
   */
  public function isLibraryInstalled(): bool;

  /**
   * Ping to validate an API key. If an API key is passed as a parameter, then
   * it tests that API key instead of the one loaded from config.
   *
   * @return bool
   *   True if API returns expected "PONG!" otherwise false
   */
  public function isApiKeyValid(?string $api_key = NULL): bool;

  /**
   * Return Mailchimp Marketing API object.
   *
   * Allows communication with the mailchimp server.
   *
   * @param bool $reset
   *   Pass in TRUE to reset the statically cached object.
   * @param string $api_key
   *   API key to authorize Mailchimp Marketing API.
   *
   * @return \MailchimpMarketing\ApiClient|bool
   *   Mailchimp Marketing Object upon success
   *   FALSE if mailchimp settings 'api_key' is unset
   *
   * @throws \MailchimpMarketing\ApiException
   */
  public function getApiObject(bool $reset = FALSE, string $api_key = ''): bool|ApiClient;

  /**
   * Return a new Mailchimp Transactional API object without looking in cache.
   *
   * @param string $api_key
   *   API key to authorize Mailchimp Transactional API.
   *
   * @throws \MailchimpMarketing\ApiException
   */
  public function getNewApiObject(string $api_key = ''): mixed;

  /**
   * Builds a customer array for Mailchimp.
   *
   * Customer, Order, and Cart endpoints all use the customer object. Provide a
   * build function in the base class to share with all three handlers.
   *
   * @param string $mail
   *   Use an email address as a unique identifier for the customer.
   * @param bool $opt_in
   *   Whether to opt in the customer for marketing emails.
   * @param \Drupal\profile\Entity\ProfileInterface|null $profile
   *   An optional profile to provide address and name data.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the customer. Limited
   *      to 50 characters.
   *   -  email_address: (string) Required. The customer's email address.
   *   -  opt_in_status: (string) Required. The customer's opt-in status. This
   *      value will never overwrite the opt-in status of a pre-existing
   *      Mailchimp list member, but will apply to list members that are added
   *      through the e-commerce API endpoints. Customers who don't opt in to
   *      your Mailchimp list will be added as Transactional members.
   *   -  company: (string) The customer's company.
   *   -  first_name: (string) The customer's first name.
   *   -  last_name: (string) The customer's last name.
   *   -  address: (object) The customer's address.
   */
  public function buildCustomerBody(string $mail, bool $opt_in = FALSE, ?ProfileInterface $profile = NULL, ?array $property_overrides = NULL): array;

  /**
   * Get the mapped configuration values from Drupal.
   *
   * @param string $entity_type_id
   * @param string $bundle_id
   * @param string $mailchimp_property_name
   *
   * @return array|string
   *
   */
  public function getPropertyMapping(string $entity_type_id, string $bundle_id, string $mailchimp_property_name): mixed;

  /**
   * Given an entity, get the value for the associated property based on config.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface|\Drupal\commerce_product\Entity\ProductVariationInterface $product
   * @param string $mailchimp_property_name
   *
   * @return mixed
   */
  public function getMappedPropertyValue(ProductVariationInterface|ProductInterface $product, string $mailchimp_property_name): mixed;

  /**
   * Creates an id to associate with a customer email address.
   * Mailchimp requires a unique customer ID with max length of 50. Instead of
   * doing anything complicated, do a Standard DES hash on the customer's
   * email address and use that as the ID.
   *
   * @param string $mail
   *   The email address.
   * @return string
   *   The new ID.
   */
  public function createIdFromMail(string $mail): string;

  /**
   * Validate that the campaign ID exists.
   *
   * @param string $campaign_id
   *   The campaign ID.
   *
   * @return bool
   *   TRUE if ID exists, FALSE if not or if there was an API error.
   */
  public function validateCampaignId(string $campaign_id): bool;

}
