<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use stdClass;

/**
 * Covers the Customer endpoint in the Mailchimp Marketing API.
 *
 * Add Customers to your Store to track their orders and to view E-Commerce
 * Data for your Mailchimp lists and campaigns. Each Customer is connected to a
 * Mailchimp list member, so adding a Customer can also add or update a list
 * member.
 *
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-customers/
 */
interface CustomerHandlerInterface {

  /**
   * Sync a customer between Drupal and Mailchimp.
   *
   * This function is a combination of API calls. First, it tries to GET the
   * object in question. If it receives a 404 error in response, the object
   * does not exist, so it calls POST. Otherwise, the object must exist,
   * so it calls PATCH.
   *
   * @param string $mail
   *   The ID of the customer to sync.
   * @param bool $opt_in
   *   Whether to opt the customer in to marketing emails.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncCustomer(string $mail, bool $opt_in = FALSE): void;

  /**
   * Sync a customer between Drupal and Mailchimp with double opt-in.
   *
   * Mailchimp offers double opt-in, which includes an extra confirmation step
   * to verify the customer’s email address. For double opt-in emails to be sent
   * properly, the contact’s status must first be set to transactional and then
   * to pending. This function follows the process outlined by Mailchimp and
   * makes a sequence of requests:
   * 1 Makes a GET request to see if the customer exists.
   * 2 Adds or updates the customer as a transactional contact with a value of
   *   false for the opt_in_status field. (Sending a value of true would add
   *   them as a subscriber and opt them into marketing emails.)
   * 3 Edits the contact by making a PATCH request to
   *   lists/{list_id}/members/{subscriber_hash} with a value of pending for the
   *   status field.
   *
   * @param string $mail
   *   The ID of the customer to sync.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncCustomerDoubleOptIn(string $mail): void;

  /**
   * Gets customers from a Mailchimp store.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   * @param string|null $email_address
   *   Restrict the response to customers with the email address.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Get by ID requests should allow errors to bubble up, since a 404 error is
   *   the only indication that the object does not exist in Mailchimp.
   */
  public function getCustomers(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0, ?string $email_address = NULL): ?stdClass;

  /**
   * Gets a single customer from a Mailchimp store.
   *
   * @param string $mail
   *   A unique ID, which should be the same in both Drupal and Mailchimp.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Get by ID requests should allow errors to bubble up, since a 404 error is
   *   the only indication that the object does not exist in Mailchimp.
   */
  public function getCustomer(string $mail, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Creates a new customer in Mailchimp based on data from Drupal.
   *
   * @param string $mail
   *   The customer id.
   * @param bool $opt_in
   *   If the customer should be automatically opted-in to marketing emails.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Get by ID requests should allow errors to bubble up, since a 404 error is
   *   the only indication that the object does not exist in Mailchimp.
   */
  public function addCustomer(string $mail, bool $opt_in = FALSE): ?stdClass;

  /**
   * Update a customer in Mailchimp based on data from Drupal.
   *
   * @param string $mail
   *   The customer id.
   * @param bool $opt_in
   *   If the customer should be automatically opted-in to marketing emails.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Get by ID requests should allow errors to bubble up, since a 404 error is
   *   the only indication that the object does not exist in Mailchimp.
   */
  public function updateCustomer(string $mail, bool $opt_in = FALSE): ?stdClass;

  /**
   * Deletes a customer in Mailchimp.
   *
   * @param string $mail
   *   The customer id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteCustomer(string $mail): void;

}
