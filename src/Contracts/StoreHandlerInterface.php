<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use stdClass;

/**
 * Covers E-Commerce Store endpoint in the Mailchimp Marketing API.
 *
 * Connect your E-commerce Store to Mailchimp to take advantage of powerful
 * reporting and personalization features and to learn more about your
 * customers.
 *
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-stores/
 */
interface StoreHandlerInterface {

  /**
   * Gets all Stores in Mailchimp.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response.
   */
  public function getStores(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a Store from Mailchimp.
   *
   * @param string $remote_store_id
   *   The Mailchimp Store ID.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Get by ID requests should allow errors to bubble up, since a 404 error is
   *   the only indication that the object does not exist in Mailchimp.
   */
  public function getStore(string $remote_store_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Create a Store in Mailchimp.
   *
   * @param array $store_body
   *   An array representation of the Store to create.
   *
   * @return object|null
   *   The API response.
   */
  public function addStore(array $store_body): ?stdClass;

  /**
   * Update a Mailchimp Store with data from Drupal.
   *
   * @param string $remote_store_id
   *   The Mailchimp Store ID.
   * @param array $body
   *   An array representation of the Store.
   *
   * @return object|null
   *   The API response.
   */
  public function updateStore(string $remote_store_id, array $body): ?stdClass;

  /**
   * Delete a Store from Mailchimp.
   *
   * @param string $remote_store_id
   *   The Mailchimp Store ID.
   */
  public function deleteStore(string $remote_store_id): void;

  /**
   * Indicate that the Store is syncing data and pause automations.
   */
  public function enableSyncing(): void;

  /**
   * Indicate that the Store is done syncing and resume automations.
   */
  public function disableSyncing(): void;

}