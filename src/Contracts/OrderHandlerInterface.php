<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use stdClass;

/**
 * Covers Cart, Cart Line, Order, and Order Line endpoints.
 *
 * In Drupal Commerce, carts and orders are the same entity type. In Mailchimp,
 * they are separate types. Due to the functional overlap, all cart and order
 * functions are contained within this single handler.
 *
 * Use Carts to represent unfinished e-commerce transactions. This can be used
 * to create an Abandoned Cart workflow, or to save a consumer’s shopping cart
 * pending a successful Order.
 *
 * Each Cart contains one or more Cart Lines, which represent a specific Product
 * Variant that a Customer has added to their shopping cart.
 *
 * Orders represent successful e-commerce transactions, and this data can be
 * used to provide more detailed campaign reports, track sales, and personalize
 * emails to your targeted consumers, and view other e-commerce data in your
 * Mailchimp account.
 *
 * Each Order contains one or more Order Lines, which represent a specific
 * Product Variant that a Customer purchases.
 *
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-carts/
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-cart-liness/
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-orders/
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-order-lines/
 */
interface OrderHandlerInterface {

  /**
   * Sync a cart between Drupal and Mailchimp.
   *
   * There is currently no PUT operation for carts in Mailchimp E-commerce API.
   * To sync a cart, we must test to see if it exists in Mailchimp with a GET
   * call. If that responds with a 404 error, we should POST/add the order,
   * otherwise, we should PATCH/update.
   *
   * @param int|string $cart_id
   *   The ID of the order in Drupal.
   * @param string|null $campaign_id
   *   Optional. The campaign ID from the user's session storage.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncCart(int|string $cart_id, ?string $campaign_id = NULL): void;

  /**
   * Sync an Order between Drupal and Mailchimp.
   *
   * There is currently no PUT operation for orders in Mailchimp E-commerce API.
   * To sync an order, we must test to see if it exists in Mailchimp with a GET
   * call. If that responds with a 404 error, we should POST/add the order,
   * otherwise, we should PATCH/update.
   *
   * @param int|string $order_id
   *   The ID of the order in Drupal.
   * @param string|null $mc_state
   *   The state of the order that needs to be sent to Mailchimp.
   * @param string|null $campaign_id
   *   Optional. The campaign ID from the user's session storage.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncOrder(int|string $order_id, ?string $mc_state = NULL, ?string $campaign_id = NULL): void;

  /**
   * Gets carts from a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getCarts(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a single cart from a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $cart_id
   *   The unique cart ID, which should be the same in Drupal and Mailchimp.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getCart(int|string $cart_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Gets line items from a cart in a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $cart_id
   *   The id of the parent cart.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getCartLines(int|string $cart_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a specific line items from a cart in a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $cart_id
   *   The id of the parent cart.
   * @param int|string $line_id
   *   The line item id.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getCartLine(int|string $cart_id, int|string $line_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Gets orders from a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getOrders(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a specific Order from a Mailchimp store.
   *  When the response to a GET is 404, it means the entity does not exist in
   *  Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $order_id
   *   The id of the order.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getOrder(int|string $order_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Gets all line items from an Order in Mailchimp.
   * When the response to a GET is 404, it means the entity does not exist in
   * Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $order_id
   *   The id of the order.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getOrderLines(int|string $order_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a specific Order line item from Mailchimp.
   * When the response to a GET is 404, it means the entity does not exist in
   * Mailchimp. Instead of treating this as an error, return NULL.
   *
   * @param int|string $order_id
   *   The id of the order.
   * @param int|string $line_id
   *   The line item id.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response. Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getOrderLine(int|string $order_id, int|string $line_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Creates a new cart in Mailchimp.
   *
   * @param int|string $cart_id
   *   The cart id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addCart(int|string $cart_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Adds a new line item to a cart in Mailchimp.
   *
   * @param int|string $cart_id
   *   The parent cart id.
   * @param int|string $line_id
   *   The cart line item id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addCartLine(int|string $cart_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Creates a new Order in Mailchimp.
   *
   * @param int|string $order_id
   *   The Order id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addOrder(int|string $order_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Adds a new line item to an Order in Mailchimp.
   *
   * @param int|string $order_id
   *   The parent Order id.
   * @param int|string $line_id
   *   The Order line item id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addOrderLine(int|string $order_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Updates a cart in Mailchimp.
   *
   * @param int|string $cart_id
   *   The cart id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateCart(int|string $cart_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Updates a cart line item in Mailchimp.
   *
   * @param int|string $cart_id
   *   The parent cart id.
   * @param int|string $line_id
   *   The cart line item id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateCartLine(int|string $cart_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Updates an Order in Mailchimp.
   *
   * @param int|string $order_id
   *   The Order id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateOrder(int|string $order_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Updates an Order line in Mailchimp.
   *
   * @param int|string $order_id
   *   The parent Order id.
   * @param int|string $line_id
   *   The Order line item id.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the order entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateOrderLine(int|string $order_id, int|string $line_id, ?array $property_overrides = NULL): ?stdClass;

  /**
   * Deletes a cart in Mailchimp.
   *
   * @param int|string $cart_id
   *   The cart id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteCart(int|string $cart_id): void;

  /**
   * Deletes a cart line item in Mailchimp.
   *
   * @param int|string $cart_id
   *   The parent cart id.
   * @param int|string $line_id
   *   The cart line item id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteCartLine(int|string $cart_id, int|string $line_id): void;

  /**
   * Deletes an Order in Mailchimp.
   *
   * @param int|string $order_id
   *   The Order id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteOrder(int|string $order_id): void;

  /**
   * Deletes an Order line item in Mailchimp.
   *
   * @param int|string $order_id
   *   The parent Order id.
   * @param int|string $line_id
   *   The Order line item id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteOrderLine(int|string $order_id, int|string $line_id): void;

  /**
   * Builds the base array for a cart or Order in Mailchimp.
   *
   * Mailchimp carts and Orders are similarly structured, but have differences.
   * This function builds the base array of elements which both share, and is
   * meant to be extended by the functions which will specifically build a cart
   * or Order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The Order.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique id for the order.
   *   -  customer: (object) Required. Information about a specific customer.
   *      For existing customers include only the id parameter in the customer
   *      object body.
   *   -  currency_code: (string) Required. The three-letter ISO 4217 code for
   *      the currency that the store accepts.
   *   -  order_total: (number) Required. The total for the order.
   *   -  lines: (object[]) Required. An array of the order's line items.
   *   -  campaign_id: (string) A string that uniquely identifies the campaign
   *      for an order.
   *   -  tax_total: (number) The tax total for the order.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildCartOrderBase(OrderInterface $order): array;

  /**
   * Builds the base array for cart and Order line items.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   An Order Item.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the Order line item.
   *   -  product_id: (string) Required. A unique identifier for the product
   *      associated with the Order line item.
   *   -  product_variant_id: (string) Required. A unique identifier for the
   *      Product Variant associated with the Order line item.
   *   -  quantity: (integer) Required. The quantity of an Order line item.
   *   -  price: (number) Required. The price of an Order line item.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildCartOrderLineBase(OrderItemInterface $order_item): array;

  /**
   * Given an Order or its ID, generate the body for a Post or Update API call.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|int|string $cart
   *   The cart Order or its ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique id for the order.
   *   -  customer: (object) Required. Information about a specific customer.
   *      For existing customers include only the id parameter in the customer
   *      object body.
   *   -  currency_code: (string) Required. The three-letter ISO 4217 code for
   *      the currency that the store accepts.
   *   -  order_total: (number) Required. The total for the order.
   *   -  lines: (object[]) Required. An array of the order's line items.
   *   -  campaign_id: (string) A string that uniquely identifies the campaign
   *      for an order.
   *   -  checkout_url: (string) The URL for the cart. This parameter is
   *      required for Abandoned Cart automations.
   *   -  tax_total: (number) The tax total for the order.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildCartBody(OrderInterface|int|string $cart, ?array $property_overrides = NULL): array;

  /**
   * Builds the request body for adding or updating a cart line item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface|int|string $line
   *   The Order Item or its ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the Order line item.
   *   -  product_id: (string) Required. A unique identifier for the product
   *      associated with the Order line item.
   *   -  product_variant_id: (string) Required. A unique identifier for the
   *      Product Variant associated with the Order line item.
   *   -  quantity: (integer) Required. The quantity of an Order line item.
   *   -  price: (number) Required. The price of an Order line item.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildCartLineBody(OrderItemInterface|int|string $line, ?array $property_overrides = NULL): array;


  /**
   * Given an Order or its ID, generate the body for a Post or Update API call.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|int|string $order
   *   The Order or its ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique id for the order.
   *   -  customer: (object) Required. Information about a specific customer.
   *      For existing customers include only the id parameter in the customer
   *      object body.
   *   -  currency_code: (string) Required. The three-letter ISO 4217 code for
   *      the currency that the store accepts.
   *   -  order_total: (number) Required. The total for the order.
   *   -  lines: (object[]) Required. An array of the order's line items.
   *   -  campaign_id: (string) A string that uniquely identifies the campaign
   *      for an order.
   *   -  landing_site: (string) The URL for the page where the buyer landed
   *      when entering the shop.
   *   -  financial_status: (string) The Order status. Use this parameter to
   *      trigger Order Notifications.
   *   -  fulfillment_status: (string) The fulfillment status for the order.
   *      Use this parameter to trigger Order Notifications.
   *   -  order_url: (string) The URL for the order.
   *   -  discount_total: (number) The total amount of the discounts to be
   *      applied to the price of the order.
   *   -  tax_total: (number) The tax total for the order.
   *   -  shipping_total: (number) The shipping total for the order.
   *   -  tracking_code: (string) The Mailchimp tracking code for the order.
   *      Uses the 'mc_tc' parameter in E-Commerce tracking URLs. Possible
   *      value: "prec".
   *   -  processed_at_foreign: (string) The date and time the Order was
   *      processed in ISO 8601 format.
   *   -  cancelled_at_foreign: (string) The date and time the Order was
   *      cancelled in ISO 8601 format. Note: passing a value for this parameter
   *      will cancel the Order being created.
   *   -  updated_at_foreign: (string) The date and time the Order was updated
   *      in ISO 8601 format.
   *   -  shipping_address: (object) The shipping address for the order.
   *   -  billing_address: (object) The billing address for the order.
   *   -  promos: (object[]) The Promo Codes applied on the order.
   *   -  outreach: (object) The outreach associated with this order.
   *      For example, an email campaign or Facebook ad.
   *   -  tracking_number: (string) The tracking number associated with the
   *      order.
   *   -  tracking_carrier: (string) The tracking carrier associated with the
   *      order.
   *   -  tracking_url: (string) The tracking URL associated with the order.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildOrderBody(OrderInterface|int|string $order, array $property_overrides = NULL): array;

  /**
   * Given an Order Item or ID, generate the body for a Post or Update API call.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface|int|string $order_item
   *   An Order Item or its ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the Order line item.
   *   -  product_id: (string) Required. A unique identifier for the product
   *      associated with the Order line item.
   *   -  product_variant_id: (string) Required. A unique identifier for the
   *      Product Variant associated with the Order line item.
   *   -  quantity: (integer) Required. The quantity of an Order line item.
   *   -  price: (number) Required. The price of an Order line item.
   *   -  discount: (number) The total discount amount applied to the line item.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildOrderLineBody(OrderItemInterface|int|string $order_item, ?array $property_overrides = NULL): array;

}
