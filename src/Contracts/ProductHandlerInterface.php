<?php

namespace Drupal\mailchimp_ecommerce_async\Contracts;

use stdClass;

/**
 * Covers Product and Product Variant endpoints in the Mailchimp Marketing API.
 *
 * E-commerce items for sale in your store need to be created as Products so you
 * can add the items to a Cart or an Order. Each Product requires at least one
 * Product Variant.
 *
 * A Product Variant represents a specific item for purchase, and is contained
 * within a parent Product. At least one Product Variant is required for each
 * Product.
 *
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-products/
 * @see https://mailchimp.com/developer/marketing/api/ecommerce-product-variants/
 */
interface ProductHandlerInterface {

  /**
   * Sync a product between Drupal and Mailchimp.
   *
   * @param int|string $product_id
   *   The ID of the product to sync.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function syncProduct(int|string $product_id): void;

  /**
   * Gets products from a Mailchimp store.
   *
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response.Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getProducts(?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a single product from a Mailchimp store.
   *
   * @param int|string $product_id
   *   A unique ID, which should be the same in both Drupal and Mailchimp.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response.Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getProduct(int|string $product_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Gets variants from a product in a Mailchimp store.
   *
   * @param int|string $product_id
   *   The parent product's ID.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   * @param int $count
   *   The number of records to return.
   *   Default value is 10. Maximum value is 1000.
   * @param int $offset
   *   Used for pagination, the number of records from a collection to skip.
   *   Default value is 0.
   *
   * @return object|null
   *   The API response.Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getProductVariants(int|string $product_id, ?array $fields = NULL, ?array $exclude_fields = NULL, int $count = 10, int $offset = 0): ?stdClass;

  /**
   * Gets a variant from a product in a Mailchimp store.
   *
   * @param int|string $product_id
   *   The parent product's ID.
   * @param int|string $variant_id
   *   A unique ID, which should be the same in both Drupal and Mailchimp.
   * @param string[]|null $fields
   *   A comma-separated list of fields to return.
   *   Reference parameters of sub-objects with dot notation.
   * @param string[]|null $exclude_fields
   *   A comma-separated list of fields to exclude.
   *   Reference parameters of sub-objects with dot notation.
   *
   * @return object|null
   *   The API response.Returns NULL if the object does not exist.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function getProductVariant(int|string $product_id, int|string $variant_id, ?array $fields = NULL, ?array $exclude_fields = NULL): ?stdClass;

  /**
   * Creates a new product in Mailchimp based on a Drupal Commerce Product ID.
   *
   * @param int|string $product_id
   *   The product id.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addProduct(int|string $product_id): ?stdClass;

  /**
   * Adds a variant to a product based on a Commerce Product Variation ID.
   *
   * @param int|string $product_id
   *   The parent product id.
   * @param int|string $variant_id
   *   The Product Variation id.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function addProductVariant(int|string $product_id, int|string $variant_id): ?stdClass;

  /**
   * Updates a product in Mailchimp based on the values in Drupal.
   *
   * @param int|string $product_id
   *   The product id.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateProduct(int|string $product_id): ?stdClass;

  /**
   * Updates a Product Variant in Mailchimp based on the values in Drupal.
   *
   * @param int|string $product_id
   *   The parent product id.
   * @param int|string $variant_id
   *   The Product Variation id.
   *
   * @return object|null
   *   The API response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *    Thrown if the entity no longer exists in Drupal.
   * @throws \GuzzleHttp\Exception\RequestException
   *   Allow request exceptions to bubble up, so that we can handle them at a
   *   higher level.
   */
  public function updateProductVariant(int|string $product_id, int|string $variant_id): ?stdClass;

  /**
   * Deletes a product in Mailchimp.
   *
   * @param int|string $product_id
   *   The product id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteProduct(int|string $product_id): void;

  /**
   * Deletes a Product Variant in Mailchimp.
   *
   * @param int|string $product_id
   *   The parent product id.
   * @param int|string $variant_id
   *   The Product Variation id.
   *
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   Thrown when the Mailchimp API is unreachable, either due to throttling or
   *   some other server error.
   */
  public function deleteProductVariant(int|string $product_id, int|string $variant_id): void;

  /**
   * Given a Product ID, generate the body for a Post or Update API call.
   *
   * @param string $product_id
   *   The Drupal Product ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the product.
   *   -  title: (string) Required. The title of a product.
   *   -  variants: (object[]) Required. An array of the product's variants.
   *      At least one variant is required for each product. A variant can use
   *      the same id and title as the parent product.
   *   -  handle: (string) The handle of a product.
   *   -  url: (string) The URL for a product.
   *   -  description: (string) The description of a product.
   *   -  type: (string) The type of product.
   *   -  vendor: (string) The vendor for a product.
   *   -  image_url: (string) The image URL for a product.
   *   -  images: (object[]) An array of the product's images.
   *   -  published_at_foreign: (string) The date and time the product was
   *      published.
   */
  public function buildProductBody(string $product_id, ?array $property_overrides = NULL): array;

  /**
   * Given a Product Variation ID, generate body for a Post or Update API call.
   *
   * @param string $variation_id
   *   The Drupal Product Variation ID.
   * @param array|null $property_overrides
   *   An array that overrides property values in the returned array.
   *
   * @return array
   *   An array containing:
   *   -  id: (string) Required. A unique identifier for the product variant.
   *   -  title: (string) Required. The title of a product variant.
   *   -  url: (string) The URL for a product variant.
   *   -  sku: (string) The stock keeping unit (SKU) of a product variant.
   *   -  price: (number) The price of a product variant.
   *   -  inventory_quantity: (integer) The inventory quantity of a product
   *      variant.
   *   -  image_url: (string) The image URL for a product variant.
   *   -  backorders: (string) The backorders of a product variant.
   *   -  visibility: (string) The visibility of a product variant.
   */
  public function buildProductVariantBody(string $variation_id, ?array $property_overrides = NULL): array;

}
