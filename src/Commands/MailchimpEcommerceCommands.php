<?php

namespace Drupal\mailchimp_ecommerce_async\Commands;

use Drush\Commands\DrushCommands;

/**
 * Provide drush commands for Mailchimp E-Commerce.
 */
class MailchimpEcommerceCommands extends DrushCommands {

  /**
   * Example of a Drush command. Get stores.
   *
   * @command mailchimp-ecommerce:store
   * @param $store_id
   * @usage drush mailchimp-ecommerce:store
   * @aliases mcec:store, mailchimp-ecommerce-store, mcecstore
   * @validate-module-enabled mailchimp_ecommerce_async
   */
  public function store(?string $store_id = NULL) {
    $api = \Drupal::service('mailchimp_ecommerce_async.store_handler');

    $this->output()->writeln("");
    if ($store_id !== NULL) {
      $store = $api->getStore($store_id);
      $this->output()->writeln("ID: " . $store->id);
      $this->output()->writeln("Name: " . $store->name);
      $this->output()->writeln("Audience ID: " . $store->list_id);
      $this->output()->writeln("Platform: " . $store->platform);
      $this->output()->writeln("");
    }
    else {
      $stores = $api->getStores();
      foreach ($stores->stores as $store) {
        $this->output()->writeln("ID: " . $store->id);
        $this->output()->writeln("Name: " . $store->name);
        $this->output()->writeln("Audience ID: " . $store->list_id);
        $this->output()->writeln("Platform: " . $store->platform);
        $this->output()->writeln("");
      }
    }
  }

}
